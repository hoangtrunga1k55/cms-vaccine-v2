﻿/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 * Tích hợp và hướng dẫn bởi https://trungtrinh.com - Website chia sẻ bách khoa toàn thư */

CKEDITOR.editorConfig = function( config ) {
        config.filebrowserImageBrowseUrl = '/laravel-filemanager?type=Images';
        config.filebrowserImageUploadUrl = '/laravel-filemanager/upload?type=Images&_token=';
        config.filebrowserBrowseUrl = '/laravel-filemanager?type=Files';
        config.filebrowserUploadUrl = '/laravel-filemanager/upload?type=Files&_token=';
        config.filebrowserFlashUploadUrl = '/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
        config.filebrowserFlashBrowseUrl = '/ckeditor/ckfinder/ckfinder.html?type=Flash';
};

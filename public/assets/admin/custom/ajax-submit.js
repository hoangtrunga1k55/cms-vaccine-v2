function submitForm(event) {
    event.preventDefault;
    $(".btn-submit").prop('disabled', true);
    clearValidateMessage();

    $.ajax({
        method: "POST",
        url: event.getAttribute('action'),
        data: new FormData(event),
        contentType: false,
        processData: false,
        success: function (data) {
            Toast.fire({
                icon: data.status,
                title: data.message
            }).then(function (){
                window.location.href = data.url;
            });

        },
        error: function (data) {
            Toast.fire({
                icon: 'error',
                title: data.responseJSON.message
            })
            $(".btn-submit").prop('disabled', false);
            showValidateMessage(data.responseJSON.errors)
        }
    })
}

function clearValidateMessage() {
    $(".is-invalid").removeClass("is-invalid");
    $(".rule").text("");
}

function showValidateMessage(obj) {
    $.each(obj, function (key, value) {
        $('#' + key).addClass('is-invalid');
        $('#rule-' + key).text(value);
    });
}

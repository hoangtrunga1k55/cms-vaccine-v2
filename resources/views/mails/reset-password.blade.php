<p>
    Vui lòng truy cập đường dẫn dưới đây để đổi lại mật khẩu của bạn.
    <br>
    Đường link sẽ hết hạn trong 48h
    <br>
    Nếu bạn không yêu cầu đổi mật khẩu thì có thể bỏ qua email này</p>
<a href="{{ route('reset_password', ['token' => $token]) }}">
    Đổi mật khẩu
</a>

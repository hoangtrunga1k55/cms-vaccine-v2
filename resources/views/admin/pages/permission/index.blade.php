@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Quyền</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Danh Sách Quyền</h3>
                    <div class="card-tools">
                        <a href="{{ route('permission.create') }}" type="button" class="btn btn-outline-success">
                            Tạo mới
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Tên</th>
                            <th>Nhóm</th>
                            <th>Hành Động</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($permissions as $permission)
                            <tr>
                                <td>
                                    {{ ($permissions->currentPage()-1) * $permissions->perPage() + $loop->iteration }}.
                                </td>
                                <td>{{ $permission->name }}</td>
                                <td>{{ $permission->group }}</td>
                                <td>
                                    <select class="form-control" onchange="redirect(this)"
                                            permission-id="{{ $permission->id }}">
                                        <option>Chọn hành động</option>
                                        @if( \App\Helpers\PermissionsHelper::can('admin.permission.edit'))
                                            <option
                                                value="edit">
                                                Sửa quền
                                            </option>
                                        @endif
                                        @if( \App\Helpers\PermissionsHelper::can('admin.permission.destroy'))
                                            <option
                                                value="destroy">
                                                Xóa quyền
                                            </option>
                                        @endif
                                    </select>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    {{ $permissions->links() }}
                </div>
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
@endsection
@section('js')
    <script>
        @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{ session('message') }}'
        });
        @endif

        function redirect(event) {
            let res, url;
            let id = event.getAttribute('permission-id');
            switch (event.value) {
                case "edit":
                    url = "{{ route("permission.edit", "%id%") }}";
                    res = url.replace('%id%', id)
                    window.location.href = res;
                    break;
                case "destroy":
                    url = "{{ route("permission.destroy", "%id%") }}";
                    res = url.replace('%id%', id);
                    confirmDelete(res);
                    break;
            }
        }
    </script>
@endsection

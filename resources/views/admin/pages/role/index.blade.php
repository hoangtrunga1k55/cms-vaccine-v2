@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Chức</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Danh Sách Chức</h3>
                    <div class="card-tools">
                        <a href="{{ route('role.create') }}" type="button" class="btn btn-outline-success">
                            Tạo mới
                        </a>
                        <a href="{{ route('role.sync') }}" type="button" class="btn btn-outline-success">
                            Khởi tại lại nhóm quyền
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Tên</th>
                            <th>Mô tả</th>
                            <th colspan="2">Hành Động</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($roles as $role)
                            <tr>
                                <td>{{ ($roles->currentPage()-1) * $roles->perPage() + $loop->iteration }}.</td>
                                <td>{{ $role->name }}</td>
                                <td>{{ $role->description }}</td>
                                <td >
                                    <select class="form-control" onchange="redirect(this)"
                                            role-id="{{ $role->id }}">
                                        <option>Chọn hành động</option>
                                        @if( \App\Helpers\PermissionsHelper::can('admin.role.edit'))
                                            <option
                                                value="edit">
                                                Sửa vai trò
                                            </option>
                                        @endif
                                        @if( \App\Helpers\PermissionsHelper::can('admin.role.destroy'))
                                            <option
                                                value="destroy">
                                                Xóa vai trò
                                            </option>
                                        @endif
                                    </select>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    {{ $roles->links() }}
                </div>
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
@endsection
@section('js')
    <script>
        @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{ session('message') }}'
        });
        @endif

        function redirect(event) {
            let res, url;
            let id = event.getAttribute('role-id');
            switch (event.value) {
                case "edit":
                    url = "{{ route("role.edit", "%id%") }}";
                    res = url.replace('%id%', id)
                    window.location.href = res;
                    break;
                case "destroy":
                    url = "{{ route("role.destroy", "%id%") }}";
                    res = url.replace('%id%', id)
                    confirmDelete(res);
                    break;
            }
        }
    </script>
@endsection

@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Role</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>


        <div class="content">
            <div class="content-fluid">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">@if(isset($role)) Sửa @else Tạo @endif chức</h3>
                    </div>
                    <form
                        @if(isset($role))
                        action="{{ route('role.update', [$role->id]) }}"
                        @else
                        action="{{ route('role.store') }}"
                        @endif
                        method="post"
                        enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="card-body row">
                            <div class="col-12 col-xl-6">
                                <div class="form-group ">
                                    <label for="name">Tên</label>
                                    <input type="text" class="form-control" name="name" id="name"
                                           placeholder="Nhập tên"
                                           value="{{ old('name', $role->name ?? null) }}">
                                    @error('name')
                                    <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12 col-xl-6">
                                <div class="form-group">
                                    <label for="description">Mô tả</label>
                                    <input type="text" class="form-control" name="description" id="description"
                                           placeholder="Nhập tên nhóm"
                                           value="{{ old('description', $role->description ?? null) }}">
                                    @error('description')
                                    <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="permissions">Quyền</label>
                                    <div class="row">
                                        <div class="col-3">
                                            <h4>Tất cả quyền</h4>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox"
                                                       id="full" onchange="$('.permission').prop('checked', this.checked);">
                                                <label class="form-check-label"
                                                       for="full">Nhận tất cả quyền</label>
                                            </div>
                                        </div>
                                        @foreach($formattedRoles as $key=>$group)
                                            <div class="col-3">
                                                <h4>{{ $key }}</h4>
                                                @foreach($group as $permission)
                                                    <div class="form-check">
                                                        @if(isset($role))
                                                            <input class="form-check-input permission" type="checkbox"
{{--                                                                   {{ in_array(old('permissions', $permission->id), $role->permissions) ? 'checked' : '' }}--}}
                                                                   {{ in_array($permission->id, old('permissions',$role->permissions)) ? "checked" : null }}
                                                                   name="permissions[]" id="{{ $permission->id }}"
                                                                   value="{{ $permission->id }}">
                                                        @else
                                                            <input class="form-check-input permission" type="checkbox"
                                                                   {{ in_array($permission->id, old('permissions',[])) ? "checked" : null }}
                                                                   name="permissions[]" id="{{ $permission->id }}"
                                                                   value="{{ $permission->id }}">
                                                        @endif
                                                        <label class="form-check-label"
                                                               for="{{ $permission->id }}">{{ $permission->name}}</label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endforeach
                                    </div>
                                    @error('permissions')
                                    <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                <!-- /.form-group -->
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/admin/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.css') }}">
    <script src="{{ asset('assets/admin/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.js') }}"></script>
    <script>

        $('.duallistbox').bootstrapDualListbox();
    </script>
@endsection

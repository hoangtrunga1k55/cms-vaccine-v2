@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Cấu Hình</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">@if(isset($config)) Sửa @else Tạo @endif Danh Mục</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form"
                              @if(isset($config))
                              action="{{ route("config.update", $config->id)}}"
                              @else
                              action="{{ route('config.store') }}"
                              @endif
                              method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="title">Tiêu Đề</label>
                                    <input type="text" class="form-control" name="title"
                                           value="{{ old('title', $config->title ?? "") }}" id="title"
                                           placeholder="Nhập Tiêu Đề">
                                    @error('title')
                                    <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="title">Key</label>
                                    <input type="text" class="form-control" name="key"
                                           value="{{ old('key', $config->key ?? "") }}" id="key"
                                           placeholder="Nhập key">
                                    @error('key')
                                    <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="type">Kiểu dữ liệu</label>
                                    <select class="form-control" id="type" name="type" onchange="switchInput(this)">
                                        @if(isset($config->type))
                                            <option
                                                value="1" {{ old('type', $config->type) === "1" ? "selected" : null }}>
                                                HTML
                                            </option>
                                            <option
                                                value="0" {{ old('type', $config->type) === "0" ? "selected" : null }}>
                                                Text
                                            </option>
                                        @else
                                            <option value="1" {{ old('type') === "0" ? "selected" : null }}>HTML
                                            </option>
                                            <option value="0" {{ old('type') === "0" ? "selected" : null }}>Text
                                            </option>
                                        @endif
                                    </select>
                                    @error('type')
                                    <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="content">Value</label>
                                    <br>
                                    <textarea name="content" class="form-control"
                                              id="content">{{ old('content', $config->content ??"") }}</textarea>

                                    @error('content')
                                    <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="status">Trạng Thái</label>
                                    <input type="hidden" class="form-control" name="status" value="0">
                                    <br>
                                    <input type="checkbox" data-toggle="toggle" class="form-control" name="status"
                                           {{$config->status??old('status')?'checked':''}} value="1">
                                </div>
                                @error('status')
                                <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">@if(isset($config)) Sửa @else
                                        Tạo @endif</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section("js")
    <script>
        $(document).ready(function () {
            if ($("#type").val() == 1) {
                CKEDITOR.replace('content');
                @if(isset($config->type) && $config->type == 1)
                    CKEDITOR.instances['content'].setData($("textarea#content").val());
                @endif
            }
        });


        function switchInput(ev) {
            if (ev.value == 1) {
                CKEDITOR.replace('content');
                CKEDITOR.instances['content'].setData($("textarea#content").val());
            } else {
                CKEDITOR.instances['content'].destroy();
            }
        }
    </script>
@endsection

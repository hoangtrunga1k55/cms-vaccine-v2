@extends('admin.layouts.master')
@section('title', "Quản lý đơn đặt hàng")
@section('content')
    <div class="content-wrapper" style="min-height: 1329.44px;">
        <section class="content-header">
            <section class="content">
                <div class="content-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h1>Danh Sách Đơn đặt hàng</h1>
                            <br>
                            <div class="card">
                                <div class="card-body">
                                    <form class="row" method="get" id="search">
                                        <div class="col-lg-3 col-6">
                                            <div class="form-group">
                                                <label for="status">Tên người đặt</label>
                                                <input type="text" class="form-control" placeholder="Tên người đặt..."
                                                       name="name" value="{{ $request->full_name ?? "" }}">
                                            </div>
                                            <!-- /.card-tools -->
                                        </div>
                                        <div class="col-12">
                                            <button type="submit" class="btn btn-success">Tìm kiếm
                                            </button>
                                            <button id="button_refresh" class="btn btn-info">Làm mới</button>
                                        </div>
                                        <!-- /.card-body -->
                                    </form>
                                    <div class="row pt-5">
                                        <p>Có tất cả {{ $orders->total() }} kết quả thỏa mãn điều kiện tìm kiếm</p>
                                        <div class="col-sm-12">
                                            <table id="example2"
                                                   class="table table-striped vertical"
                                                   role="grid">
                                                <thead>
                                                <tr role="row">
                                                    <th>#</th>
                                                    <th class="w-auto">Tên người tiêm</th>
                                                    <th class="w-auto">SĐT</th>
                                                    <th class="w-auto">Email</th>
                                                    <th>Trạng thái</th>
                                                    <th class="w-auto">Thời gian tạo</th>
                                                    {{--                                                    <th>Hành động</th>--}}
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($orders as $order)
                                                    <tr class="even">
                                                        <td>
                                                            #{{ ($orders->currentPage()-1) * $orders->perPage() + $loop->iteration }}
                                                        </td>
                                                        <td onclick="detail('{{ $order->id }}')">
                                                            <a href="javascript:void(0)">{{$order->full_name }}</a>
                                                        </td>
                                                        <td>
                                                            <a href="tel:{{$order->phone_number}}">{{$order->phone_number}}</a>
                                                        </td>
                                                        <td>{{$order->email }}</td>
                                                        <td>
                                                            <select order-id="{{ $order->id }}"
                                                                    class="form-control"
                                                                    onchange="updateStatus(this)">
                                                                <option>Chọn trạng thái</option>
                                                                <option
{{--                                                                    @if(\App\Helpers\PermissionsHelper::can("order.finish-status")) disabled @endif--}}
                                                                    value="{{ App\Models\Order::STATUS_FINISH }}"
                                                                        @if( $order->status == App\Models\Order::STATUS_FINISH) selected @endif>
                                                                    {{ App\Models\Order::STATUS[2] }}
                                                                </option>
                                                                <option
{{--                                                                    @if(\App\Helpers\PermissionsHelper::can("order.failed-status")) disabled @endif--}}
                                                                        value="{{ App\Models\Order::STATUS_FAILED }}"
                                                                        @if( $order->status == App\Models\Order::STATUS_FAILED) selected @endif>
                                                                    {{ App\Models\Order::STATUS[3] }}
                                                                </option>
                                                            </select>
                                                        </td>
                                                        <td>{{$order->created_at->format("H:m d/m/Y") }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    {{ $orders->appends(request()->query())->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </div>
@endsection

@section('modals')
    <!-- Chi tiết tin tức -->
    <div class="modal fade" id="detailOrder" tabindex="-1" role="dialog" aria-labelledby="detailNews"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" id="modal_content">
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{ session('message') }}'
        });
        @endif

        function detail(id) {
            let res, url;
            url = "{{ route("order.detail", "%id%") }}";
            res = url.replace('%id%', id)
            $.ajax({
                method: "GET",
                url: res,
                success: function (data) {
                    $("#modal_content").html(data.view);
                    $('#detailOrder').modal('show');
                },
                error: function (data) {
                    Toast.fire({
                        icon: "error",
                        title: data.responseJSON.message
                    });
                }
            });
        }

        function updateStatus(event) {
            let res, url;
            let id = event.getAttribute('order-id');

            switch (event.value) {
                case "{{ \App\Models\Order::STATUS_FINISH }}":
                    url = "{{ route("order.finish-status", "%id%") }}";
                    res = url.replace('%id%', id);
                    break;
                case "{{ \App\Models\Order::STATUS_FAILED }}":
                    url = "{{ route("order.failed-status", "%id%") }}";
                    res = url.replace('%id%', id);
                    console.log(res);
                    break;
            }
            if (!!res) {
                $.ajax({
                    method: "POST",
                    url: res,
                    success: function (data) {
                        Toast.fire({
                            icon: data.status,
                            title: data.message
                        });
                    },
                    error: function (data) {
                        Toast.fire({
                            icon: "error",
                            title: data.responseJSON.message
                        });
                    }
                })
            }
        }
    </script>
@endsection



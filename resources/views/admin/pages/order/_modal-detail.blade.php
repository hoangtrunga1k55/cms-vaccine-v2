<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Chi tiết đơn hàng</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <strong>Tên người tiêm:</strong> {{ $order->full_name ?? "-" }}<br>
    <strong>Giới tính:</strong> {{ $order->gender ?? "-" }}<br>
    <strong>Ngày sinh:</strong> {{ $order->birth_day ?? "-" }}<br>
    <strong>Mã sổ tiêm chủng:</strong> {{ $order->orderInfo()["code_note"] ?? "-" }}<br>
    <strong>Điện thoại đăng ký:</strong> {{ $order->phone_number ?? "-" }}<br>
    <strong>Email đăng ký:</strong> {{ $order->email ?? "-" }}<br>
    <strong>Địa chỉ:</strong> {{ $order->address ?? "-" }}<br>
    <strong>Tỉnh thành:</strong> {{ $order->province ? $order->province->name_with_type : "-" }}<br>
    <strong>Quận/Huyện:</strong> {{ $order->district ? $order->district->name_with_type : "-" }}<br>
    <strong>Phường/Xã:</strong> {{ $order->ward ? $order->ward->name_with_type : "-"  }}<br>
    <strong>Người liên hệ 1:</strong> <br>
    <ul>
        <li>Họ tên: {{ $order->orderInfo()["contact_person_1_name"] ?? "-" }}</li>
        <li>SĐT: {{ $order->orderInfo()["contact_person_1_phone_number"] ?? "-" }}</li>
        <li>Quan hệ: {{ $order->orderInfo()["contact_person_1_relationship"] ?? "-" }}</li>
    </ul>
    <strong>Người liên hệ 2:</strong> <br>
    <ul>
        <li>Họ tên: {{ $order->orderInfo()["contact_person_2_name"] ?? "-" }}</li>
        <li>SĐT: {{ $order->orderInfo()["contact_person_2_phone_number"] ?? "-" }}</li>
        <li>Quan hệ: {{ $order->orderInfo()["contact_person_2_relationship"] ?? "-" }}</li>
    </ul>
    <strong>Loại mũi tiêm:</strong> {{ $order->orderInfo()["type_injection"] ?? "-" }}<br>
    <strong>Cơ sở tiêm chủng:</strong> {{ $order->orderInfo()["location"] ?? "-" }}<br>
    <strong>Danh mục mũi tiêm:</strong> {{ $order->orderInfo()["pathogen"] ?? "-" }}<br>
    <strong>Ngày tiêm:</strong> {{ $order->orderInfo()->booking_date ?? "-" }}<br>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
</div>

@extends('admin.layouts.master')
@section('title', "Quản lý bệnh lý")
@section('content')
    <div class="content-wrapper" style="min-height: 1329.44px;">
        <section class="content-header">
            <section class="content">
                <div class="content-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h1>Danh Sách Bệnh</h1>
                            <br>
                            <div class="card">
                                <div class="card-body">
                                    <form class="row" method="get" id="search">
                                        <div class="col-lg-3 col-6">
                                            <div class="form-group">
                                                <label for="status">Tên bệnh lý</label>
                                                <input type="text" class="form-control" placeholder="Tên bệnh lý..."
                                                       name="name" value="{{ $request->name ?? "" }}">
                                            </div>
                                            <!-- /.card-tools -->
                                        </div>
                                        <div class="col-12">
                                            <button type="submit" class="btn btn-success">Tìm kiếm
                                            </button>
                                            <button id="button_refresh" class="btn btn-info">Làm mới</button>
                                        </div>
                                        <!-- /.card-body -->
                                    </form>
                                    <div class="row pt-5">
                                        <p>Có tất cả {{ $pathogens->total() }} kết quả thỏa mãn điều kiện tìm kiếm</p>
                                        <div class="col-sm-12">
                                            <table id="example2"
                                                   class="table table-striped vertical"
                                                   role="grid">
                                                <thead>
                                                <tr role="row">
                                                    <th class="w-auto">#</th>
                                                    <th>Tên bệnh</th>
                                                    <th class="w-auto">Thời gian tạo</th>
                                                    <th class="w-auto">Thời gian cập nhật cuối</th>
                                                    <th class="w-auto">Hành động</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($pathogens as $pathogen)
                                                    <tr class="even">
                                                        <td>
                                                            {{ ($pathogens->currentPage()-1) * $pathogens->perPage() + $loop->iteration }}
                                                            .
                                                        </td>
                                                        <td>{{$pathogen->name}}</td>
                                                        <td>{{$pathogen->created_at}}</td>
                                                        <td>{{$pathogen->updated_at}}</td>
                                                        <td>
                                                            <select class="form-control" onchange="redirect(this)"
                                                                    pathogen-id="{{ $pathogen->id }}">
                                                                <option>Chọn hành động</option>
                                                                @if( \App\Helpers\PermissionsHelper::can('admin.pathogen.edit'))
                                                                    <option
                                                                        value="edit">
                                                                        Sửa bệnh lý
                                                                    </option>
                                                                @endif
                                                                @if( \App\Helpers\PermissionsHelper::can('admin.pathogen.destroy'))
                                                                    <option
                                                                        value="destroy">
                                                                        Xóa bệnh lý
                                                                    </option>
                                                                @endif
                                                            </select>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    {{ $pathogens->appends(request()->query())->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </div>
@endsection
@section('js')
    <script>
        @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{ session('message') }}'
        });
        @endif

        function redirect(event) {
            let res, url;
            let id = event.getAttribute('pathogen-id');
            switch (event.value) {
                case "edit":
                    url = "{{ route("pathogen.edit", "%id%") }}";
                    res = url.replace('%id%', id)
                    window.location.href = res;
                    break;
                case "destroy":
                    url = "{{ route("pathogen.destroy", "%id%") }}";
                    res = url.replace('%id%', id)
                    confirmDelete(res);
                    break;
            }
        }
    </script>
@endsection



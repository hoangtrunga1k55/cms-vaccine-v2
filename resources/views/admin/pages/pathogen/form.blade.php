@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Bệnh lý</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="content">
            <div class="content-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">@if(isset($pathogen)) Sửa @else Thêm @endif thông tin bệnh lý</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form role="form"
                                  @if(isset($pathogen))
                                  action="{{ route("pathogen.update", $pathogen->id)}}"
                                  @else
                                  action="{{ route('pathogen.store') }}"
                                  @endif
                                  onsubmit="submitForm(this); return false;"
                                  method="post" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="name">Tên bệnh</label>
                                        <input type="text" class="form-control" name="name"
                                               value="{{ old('name', $pathogen->name ?? "") }}" id="name"
                                               placeholder="Nhập tên bệnh">
                                        <small class="text-danger rule" id="rule-name"></small>

                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">@if(isset($pathogen)) Sửa @else
                                            Tạo @endif</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script src="{{ asset('assets/admin/custom/ajax-submit.js') }}"></script>
@endsection

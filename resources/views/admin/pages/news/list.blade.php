@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1329.44px;">
        <section class="content-header">
            <section class="content">
                <div class="content-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h1>Danh Sách Tin Tức</h1>
                            <br>
                            <div class="card">
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <form class="row">
                                        <div class="col-6 col-xl-3">
                                            <div class="form-group ">
                                                <label>Tiêu đề</label>
                                                <input type="text" class="form-control" name="title" id="title"
                                                       placeholder="Nhập tiêu đề" value="{{$request->title}}">
                                            </div>
                                            <div class="form-group ">
                                                <label for="name">Tên người viết</label>
                                                <input type="text" class="form-control" name="name" id="name"
                                                       placeholder="Nhập tên người viết" value="{{$request->name}}">
                                            </div>
                                        </div>
                                        <div class="col-6 col-xl-3">
                                            <div class="form-group ">
                                                <label for="category">Danh mục</label>
                                                <select name="category" id="category" class="form-control">
                                                    <option value="">Chọn danh mục</option>
                                                    @foreach($categories as $category)
                                                        <option
                                                            value="{{$category->id}}" {{($category->id==$request->category)?'selected':''}}>{{$category->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="filter_status">Trạng thái</label>
                                                <select name="status" id="filter_status" class="form-control">
                                                    <option value="">Chọn trạng thái</option>
                                                    @foreach(\App\Models\News::STATUS as $key => $status)
                                                        <option
                                                            value="{{ $key }}" {{($request->status == $key)?'selected':''}}>
                                                            {{ $status }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6 col-xl-3">
                                            <div class="form-group">
                                                <label for="approved_form">Thời gian duyệt từ</label>
                                                <input type="datetime-local" class="form-control"
                                                       name="approved_form"
                                                       id="approved_form"
                                                       value="{{ !is_null($request->approved_form) ? \Carbon\Carbon::createFromTimestamp($request->approved_form)->format('Y-m-d\TH:i') : null }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="approved_to">Thời gian duyệt đến</label>
                                                <input type="datetime-local" class="form-control"
                                                       name="approved_to"
                                                       id="approved_to"
                                                       value="{{ !is_null($request->approved_to) ?\Carbon\Carbon::createFromTimestamp($request->approved_to)->format('Y-m-d\TH:i') : null }}">
                                            </div>
                                        </div>
                                        <div class="col-6 col-xl-3">
                                            <div class="form-group">
                                                <label for="published_form">Thời gian công bố từ</label>
                                                <input type="datetime-local" class="form-control"
                                                       name="published_form"
                                                       id="published_form"
                                                       value="{{ !is_null($request->published_form) ? \Carbon\Carbon::createFromTimestamp($request->published_form)->format('Y-m-d\TH:i') : null }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="published_to">Thời gian công bố đến</label>
                                                <input type="datetime-local" class="form-control"
                                                       name="published_to"
                                                       id="published_to"
                                                       value="{{ !is_null($request->published_to) ? \Carbon\Carbon::createFromTimestamp($request->published_to)->format('Y-m-d\TH:i') : null }}">
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <button type="submit" class="btn btn-success">Tìm kiếm
                                            </button>
                                            <button id="button_refresh" class="btn btn-info">Làm mới</button>
                                        </div>
                                    </form>
                                    <div class="row pt-5">
                                        <div class="col-sm-12">
                                            <p>Có tất cả {{ $news->total() }} kết quả thỏa mãn điều kiện tìm kiếm</p>
                                            <table id="example2"
                                                   class="table table-striped"
                                                   role="grid">
                                                <thead>
                                                <tr role="row">
                                                    <th>#</th>
                                                    <th> Tiêu Đề</th>
                                                    <th> Danh Mục</th>
                                                    <th> Trạng Thái</th>
                                                    <th> Người Đăng</th>
                                                    <th> Hành Động</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @forelse($news as $new)
                                                    <tr>
                                                        <td>
                                                            #{{ ($news->currentPage()-1) * $news->perPage() + $loop->iteration }}</td>
                                                        <td news-id="{{ $new->id }}"
                                                            onclick="viewDetail(this)"><a
                                                                href="javascript:void(0)">{{$new->title ?? ""}}</a></td>
                                                        <td>@foreach($new->categories as $category)
                                                                <a href="{{ route("news.index", ['category' => $category->id]) }}">{{ $category->name ?? ""}}</a>
                                                                @if(!$loop->last), @endif
                                                            @endforeach
                                                        </td>
                                                        <td>
                                                            @if(\App\Helpers\PermissionsHelper::can('news.approve') && \App\Helpers\PermissionsHelper::can('news.publish'))
                                                                <select news-id="{{ $new->id }}"
                                                                        class="form-control"
                                                                        onchange="changeStatus(this)">
                                                                    <option value="{{ \App\Models\News::WAIT_REVIEW }}"
                                                                            @if($new->status == \App\Models\News::WAIT_REVIEW) selected @endif>
                                                                        {{ \App\Models\News::STATUS[1] }}
                                                                    </option>
                                                                    @if( \App\Helpers\PermissionsHelper::can('news.approve'))
                                                                        <option
                                                                            value="{{ \App\Models\News::WAIT_RELEASE }}"
                                                                            @if($new->status == \App\Models\News::WAIT_RELEASE) selected @endif>
                                                                            {{ \App\Models\News::STATUS[2] }}
                                                                        </option>
                                                                    @endif
                                                                    @if( \App\Helpers\PermissionsHelper::can('news.publish'))
                                                                        <option value="{{ \App\Models\News::RELEASE }}"
                                                                                @if($new->status == \App\Models\News::RELEASE) selected @endif>
                                                                            {{ \App\Models\News::STATUS[3] }}
                                                                        </option>
                                                                    @endif
                                                                </select>
                                                            @else
                                                                {{ $new->getStatus() }}
                                                            @endif
                                                        </td>
                                                        <td>
                                                            {{ $new->author->full_name ?? ($new->author->email ?? "") }}
                                                        </td>
                                                        <td>
                                                            <select class="form-control" onchange="redirect(this)"
                                                                    news-id="{{ $new->id }}">
                                                                <option readonly>Chọn hành động</option>
                                                                @if( \App\Helpers\PermissionsHelper::can('news.edit'))
                                                                    <option
                                                                        value="edit">
                                                                        Sửa bài viết
                                                                    </option>
                                                                @endif
                                                                @if( \App\Helpers\PermissionsHelper::can('news.destroy'))
                                                                    <option
                                                                        value="destroy">
                                                                        Xóa bài viết
                                                                    </option>
                                                                @endif
                                                            </select>
                                                        </td>
                                                    </tr>
                                                @empty
                                                    <tr>
                                                        <td colspan="6">Không tồn tại bản ghi nào</td>
                                                    </tr>
                                                @endforelse

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    {{ $news->appends(request()->query())->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </div>
@endsection

@section('modals')
    <!-- Chi tiết tin tức -->
    <div class="modal fade" id="detailNews" tabindex="-1" role="dialog" aria-labelledby="detailNews" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Chi tiết tin tức</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <strong>Tên người viết:</strong> <span id="modal_name"></span><br>
                    <strong>Thời gian duyệt:</strong> <span id="modal_approved_at"></span><br>
                    <strong>Thời gian công bố:</strong> <span id="modal_published_at"></span><br>
                    <strong>Danh mục:</strong> <span id="modal_categories_name"></span><br>
                    <strong>Tên người duyệt:</strong> <span id="modal_approve_by"></span><br>
                    <strong>Nội dung:</strong>
                    <p id="modal_content"></p>
                </div>
                <div class="modal-footer">
                    @if( \App\Helpers\PermissionsHelper::can('news.approve'))
                        <button class="btn btn-success btn-modal" news-id=""
                                onclick="changeStatus(this, true)"
                                value="{{ \App\Models\News::WAIT_RELEASE }}">
                            Duyệt bài viết
                        </button>
                    @endif
                    @if( \App\Helpers\PermissionsHelper::can('news.publish'))
                        <button class="btn btn-success btn-modal" news-id=""
                                onclick="changeStatus(this, true)"
                                value="{{ \App\Models\News::RELEASE }}">
                            Công bố bài viết
                        </button>
                    @endif

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{ session('message') }}'
        });
        @endif

        function changeStatus(event, reload = false) {
            $(event).prop('disabled', true);
            console.log(event.getAttribute('news-id'), event.value)
            $.ajax({
                method: "POST",
                url: "{{ route('news.update-status') }}",
                data: {
                    id: event.getAttribute('news-id'),
                    status: event.value,
                },
                success: function (data) {
                    $(event).prop('disabled', false);
                    Toast.fire({
                        icon: data.status,
                        title: data.message
                    }).then(function () {
                        if (reload) {
                            location.reload();
                        }
                    });
                },
                error: function (data) {
                    $(event).prop('disabled', false);
                    Toast.fire({
                        icon: "error",
                        title: data.responseJSON.message
                    });
                }
            })
        }

        function viewDetail(event) {
            $(event).prop('disabled', true);
            $.ajax({
                method: "GET",
                url: "{{ route('news.show') }}",
                data: {
                    id: event.getAttribute('news-id'),
                },
                success: function (data) {
                    $(event).prop('disabled', false);
                    $("#modal_name").text(data.news.full_name);
                    $("#modal_approve_by").text(data.news.approve_by);
                    $("#modal_approved_at").text(data.news.approved_at);
                    $("#modal_published_at").text(data.news.published_at);
                    $("#modal_categories_name").text(data.news.categories_name);
                    $("#modal_content").html(data.news.content);
                    $(".btn-modal").attr('news-id', data.news._id);
                    $("#url-src").val(data.news.url_for_client);
                    $('#detailNews').modal('show');
                },
                error: function (data) {
                    $(event).prop('disabled', false);
                    Toast.fire({
                        icon: "error",
                        title: data.responseJSON.message
                    });
                }
            })
        }

        function redirect(event) {
            let res, url;
            let id = event.getAttribute('news-id');
            switch (event.value) {
                case "edit":
                    url = "{{ route("news.edit", "%id%") }}";
                    res = url.replace('%id%', id)
                    window.location.href = res;
                    break;
                case "destroy":
                    url = "{{ route("news.destroy", "%id%") }}";
                    res = url.replace('%id%', id)
                    confirmDelete(res);
                    break;
            }
        }

        function copyLink() {
            let copyText = document.getElementById("url-src");
            copyText.select();
            copyText.setSelectionRange(0, 99999)
            document.execCommand("copy");

            Toast.fire({
                icon: "success",
                title: "Copy thành công"
            });
        }
    </script>
@endsection


@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Thông báo</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="content">
            <div class="content-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Tạo Thông báo</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form action="{{ route('notification.store') }}"
                                  method="post" role="form"
                                  onsubmit="submitForm(this); return false;"
                                  enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="title">Tiêu đề</label>
                                        <input type="text" class="form-control" name="title"
                                               value="{{ old('title')}}" id="title"
                                               placeholder="Nhập Tiêu Đề">
                                        <small class="text-danger rule" id="rule-title"></small>
                                    </div>
                                    <div class="form-group">
                                        <label for="body">Mô tả</label>
                                        <input type="text" class="form-control" name="body"
                                               value="{{ old('body') }}"
                                               id="body"
                                               placeholder="Nhập Mô Tả">
                                        <small class="text-danger rule" id="rule-body"></small>
                                    </div>
                                    <div class="form-group">
                                        <label>Thời gian thông báo</label>
                                        <input type="datetime-local" class="form-control" name="time"
                                               value="{{ old('time') }}" id="time"
                                               placeholder="Nhập Thời Gian Thông Báo">
                                    </div>
                                    <div class="form-group">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="now"
                                                   id="now" {{ old('now') ? "checked" : "" }}
                                                   onchange="checkTimeSend(this)">
                                            <label class="form-check-label">Gửi thông báo ngay</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Kiểu thông báo</label>
                                        <select class="form-control" id="type" name="type" onchange="checkVisible()">
                                            <option value="topic" {{ old('type') === "topic" ? "selected" : null }}>
                                                Topic
                                            </option>
                                            <option value="device" {{ old('type') === "device" ? "selected" : null }}>
                                                Khách lẻ
                                            </option>
                                        </select>
                                        <small class="text-danger rule" id="rule-type"></small>
                                    </div>

                                    <div class="form-group topic_type" id="topic_select">
                                        <label>Topic</label>
                                        <select class="form-control" id="topic" name="topic">
                                            <option
                                                value="topics-all" {{ old('topic') === "topics-all" ? "selected" : null }}>
                                                Tất cả
                                            </option>
                                            <option
                                                value="topics-android" {{ old('topic') === "topics-android" ? "selected" : null }}>
                                                Android
                                            </option>
                                            <option
                                                value="topics-ios" {{ old('topic') === "topics-ios" ? "selected" : null }}>
                                                iOS
                                            </option>
                                        </select>
                                        <small class="text-danger rule" id="rule-status"></small>
                                    </div>
                                    <div class="form-group topic_type" id="user_ids_select">
                                        <label for="user_ids">Khách</label>
                                        <select class="duallistbox" multiple="multiple" name="user_ids[]" id="user_ids">
                                            @foreach($users as $user)
                                                <option value="{{ $user->id }}"
                                                    {{ in_array($user->id , old('user_ids', [])) ? "selected" : null }}>
                                                    {{ $user->name ?? "-"}} / {{ $user->email ?? "-" }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-submit"> Tạo</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
        <script src="{{ asset('assets/admin/custom/ajax-submit.js') }}"></script>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/admin/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.css') }}">
    <script src="{{ asset('assets/admin/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.js') }}"></script>
    <script>
        $('.duallistbox').bootstrapDualListbox();

        checkVisible();

        function checkVisible() {
            let type = $('#type').val();
            $(".topic_type").hide();
            switch (type) {
                case "topic":
                    $("#topic_select").show();
                    break;
                case "device":
                    $("#user_ids_select").show();
                    break;
            }
        }

        function checkTimeSend(event) {
            console.log(event.checked);
            $("#time").attr('disabled', event.checked);
        }
    </script>
@endsection

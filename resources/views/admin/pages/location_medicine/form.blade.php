@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Phòng tiêm</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">@if(isset($medicine_location)) Sửa @else Tạo @endif Phòng
                                tiêm</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form @if(isset($medicine_location))
                              action="{{ route('medicine-location.update', [$medicine_location->id]) }}"
                              @else
                              action="{{ route('medicine-location.store') }}"
                              @endif
                              method="post" role="form"
                              onsubmit="submitForm(this); return false;"
                              enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="card-body">
                                <div class="form-group col-xs-12">
                                    <label for="name">Tên phòng tiêm</label>
                                    <input type="text" required class="form-control" id="name" name="name"
                                           placeholder="Tên phòng tiêm"
                                           value="{{ isset($medicine_location->name) ? $medicine_location->name : old('name')}}">
                                    <small class="text-danger rule" id="rule-name"></small>
                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="phone_number">Số điện thoại</label>
                                    <input type="text" required class="form-control" id="phone_number"
                                           name="phone_number" placeholder="Số điện thoại"
                                           value="{{ isset($medicine_location->phone_number) ? $medicine_location->phone_number : old('phone')}}">
                                    <small class="text-danger rule" id="rule-phone_number"></small>

                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="name">Chọn vị trí *</label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input id="pac-input" class="form-control" style="margin-bottom: 10px"
                                                   type="text" placeholder="Tìm kiếm vị trí..."/>
                                            <div id="map" style="width: 100%; height: 80vh"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="name">Latitude</label>
                                    <input type="number" readonly class="form-control" id="lat" name="lat"
                                           placeholder="Latitude"
                                           value="{{ $medicine_location->location['x'] ?? "21.0277644" }}"/>
                                    <small class="text-danger rule" id="rule-lat"></small>
                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="name">Longitude</label>
                                    <input type="number" readonly class="form-control" id="lng" name="lng"
                                           placeholder="Longitude"
                                           value="{{ $medicine_location->location['y'] ?? "105.8341598" }}"/>
                                    <small class="text-danger rule" id="rule-lng"></small>
                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="description">Mô tả</label>
                                    <textarea class="form-control" id="description"
                                              name="description"
                                              placeholder="Mô tả">{{ $medicine_location->description ?? null }}</textarea>
                                    <small class="text-danger rule" id="rule-description"></small>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-submit">@if(isset($medicine_location))
                                        Sửa @else Tạo @endif</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script src="{{ asset('assets/admin/custom/ajax-submit.js') }}
        "></script>
    <script src="{{ asset('assets/admin/plugins/moment/min/moment.min.js') }}
        "></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHB8OcAlosjUgK3gNXS9P2cSBXsi0gTgg&libraries=places"
        async defer></script>

    <script type="text/javascript">
        var map, searchBox, Marker;
        $(document).ready(function () {
            let interval = setInterval(() => {
                if (google) {
                    initMap();
                    clearInterval(interval);
                }
            }, 100);
        });

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'),
                {
                    zoom: 14.75,
                    center: {lat: Number($('#lat').val()), lng: Number($('#lng').val())}
                }
            );
            Marker = new google.maps.Marker({
                map: map,
                draggable: true,
                position: {lat: Number($('#lat').val()), lng: Number($('#lng').val())},
            });
            if ($('#lat').val() == 0) {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        var pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };

                        Marker.setPosition(pos);
                        setPosition(position.coords.latitude, position.coords.longitude);
                        map.setCenter(pos);
                    });
                }
            }
            let input = document.getElementById('pac-input');
            searchBox = new google.maps.places.SearchBox(input);
            map.addListener('bounds_changed', () => {
                searchBox.setBounds(map.getBounds());
            });

            searchBox.addListener('places_changed', () => {
                let places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }
                let bounds = new google.maps.LatLngBounds();
                places.forEach((place) => {
                    if (!place.geometry) {
                        console.log('Returned place contains no geometry');
                        return;
                    }
                    Marker.setPosition(place.geometry.location);
                    setPosition(place.geometry.location.lat(),
                        place.geometry.location.lng());
                    if (place.geometry.viewport) {
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });

            google.maps.event.addListener(this.Marker, 'dragend', (event) => {
                setPosition(event.latLng.lat(), event.latLng.lng());
            });


        }

        function setPosition(lat, lng) {
            $('#lat').val(lat);
            $('#lng').val(lng);
        }
    </script>
@endsection

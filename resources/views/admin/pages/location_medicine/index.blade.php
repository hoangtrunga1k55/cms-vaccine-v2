@extends('admin.layouts.master')
@section('title', "Quản lý bệnh lý")
@section('content')
    <div class="content-wrapper" style="min-height: 1329.44px;">
        <section class="content-header">
            <section class="content">
                <div class="content-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h1>Danh Sách phòng tiêm</h1>
                            <br>
                            <div class="card">
                                <div class="card-body">
                                    <form class="row" method="get" id="search">
                                        <div class="col-lg-3 col-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label for="status">Tên phòng tiêm</label>
                                                <input type="text" class="form-control" placeholder="Tên Phòng tiêm..."
                                                       name="name" value="{{ $request->name ?? "" }}">

                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label for="status">SDT phòng tiêm</label>
                                                <input type="text" class="form-control" placeholder="SDT phòng tiêm..."
                                                       name="sdt" value="{{ $request->sdt ?? "" }}">

                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <button type="submit" form="search" class="btn btn-success">Tìm kiếm
                                            </button>
                                            <button id="button_refresh" class="btn btn-info">Làm mới</button>
                                        </div>
                                    </form>
                                    <div class="row pt-5">
                                        <p>Có tất cả {{ $medicine_locations->total() }} kết quả thỏa mãn điều kiện tìm
                                            kiếm</p>
                                        <div class="col-sm-12">
                                            <table id="example2"
                                                   class="table table-striped vertical"
                                                   role="grid">
                                                <thead>
                                                <tr role="row">
                                                    <th class="w-auto">#</th>
                                                    <th>Tên phòng tiêm</th>
                                                    <th class="w-auto">SĐT</th>
                                                    <th>Mô tả</th>
                                                    <th>Thời gian tạo</th>
                                                    <th>Thời gian cập nhật</th>
                                                    <th class="w-auto">Hành động</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($medicine_locations as $medicine_location)
                                                    <tr class="even">
                                                        <td>
                                                            {{ ($medicine_locations->currentPage()-1) * $medicine_locations->perPage() + $loop->iteration }}
                                                            .
                                                        </td>
                                                        <td>{{$medicine_location->name}}</td>
                                                        <td>{{$medicine_location->phone_number}}</td>
                                                        <td>{{$medicine_location->description}}</td>
                                                        <td>{{$medicine_location->created_at}}</td>
                                                        <td>{{$medicine_location->updated_at}}</td>
                                                        <td>
                                                            <select class="form-control" onchange="redirect(this)"
                                                                    medicine_location_id="{{ $medicine_location->id }}">
                                                                <option>Chọn hành động</option>
                                                                @if( \App\Helpers\PermissionsHelper::can('admin.medicine_location.edit'))
                                                                    <option
                                                                        value="edit">
                                                                        Sửa phòng t
                                                                    </option>
                                                                @endif
                                                                @if( \App\Helpers\PermissionsHelper::can('admin.medicine_location.destroy'))
                                                                    <option
                                                                        value="destroy">
                                                                        Xóa phòng tiêm
                                                                    </option>
                                                                @endif
                                                            </select>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>

                                </div>
                                <div class="card-footer">
                                    {{ $medicine_locations->appends(request()->query())->links() }}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </section>
    </div>
@endsection
@section('js')
    <script>
        @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{ session('message') }}'
        });
        @endif

        function redirect(event) {
            let res, url;
            let id = event.getAttribute('medicine_location_id');
            switch (event.value) {
                case "edit":
                    url = "{{ route("medicine-location.edit", "%id%") }}";
                    res = url.replace('%id%', id)
                    window.location.href = res;
                    break;
                case "destroy":
                    url = "{{ route("medicine-location.destroy", "%id%") }}";
                    res = url.replace('%id%', id)
                    confirmDelete(res);
                    break;
            }
        }
    </script>
@endsection



@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1329.44px;">
        <section class="content-header">
            <section class="content">
                <div class="row">
                    <div class="col-12">
                        <h1>Tài khoản quản trị</h1>
                        <br>
                        <div class="card">
                            <!-- /.card-header -->
                            <div class="card-body">
                                <form class="row">
                                    <div class="col-6 col-xl-3">
                                        <div class="form-group ">
                                            <label>Email</label>
                                            <input type="text" class="form-control" name="email" id="email"
                                                   placeholder="Nhập Email" value="{{$request->email}}">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-success">Tìm kiếm
                                        </button>
                                        <button id="button_refresh" class="btn btn-info">Làm mới</button>
                                    </div>
                                </form>
                                <div class="row pt-5">
                                    <p>Có tất cả {{ $cmsAccounts->total() }} kết quả thỏa mãn điều kiện tìm kiếm</p>
                                    <div class="col-sm-12">
                                        <table id="example2"
                                               class="table table-striped vertical"
                                               role="grid">
                                            <thead>
                                            <tr>
                                                <th style="width: 10px">#</th>
                                                <th>Họ tên</th>
                                                <th>Email</th>
                                                <th>Trạng Thái</th>
                                                <th>Nhóm quyền</th>
                                                <th>Hành Động</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($cmsAccounts as $cmsAccount)
                                                <tr>
                                                    <td>
                                                        {{ ($cmsAccounts->currentPage()-1) * $cmsAccounts->perPage() + $loop->iteration }}.
                                                    </td>
                                                    <td>{{ $cmsAccount->full_name }}</td>
                                                    <td>{{ $cmsAccount->email }}</td>
                                                    <td><span
                                                            class="badge badge-{{ $cmsAccount->getBadgeColor() }}">{{ $cmsAccount->getStatus() }}</span>
                                                    </td>
                                                    <td>
                                                        @foreach($cmsAccount->roles as $role)
                                                            {{ $role->name }}@if(!$loop->last), @endif
                                                        @endforeach</td>
                                                    <td>
                                                        <select class="form-control" onchange="redirect(this)"
                                                                cms-account-id="{{ $cmsAccount->id }}">
                                                            <option>Chọn hành động</option>
                                                            @if( \App\Helpers\PermissionsHelper::can('admin.cmsAccount.edit'))
                                                                <option
                                                                    value="edit">
                                                                    Sửa tài khoản
                                                                </option>
                                                            @endif
                                                            @if( \App\Helpers\PermissionsHelper::can('admin.cmsAccount.destroy'))
                                                                <option
                                                                    value="destroy">
                                                                    Xóa tài khoản
                                                                </option>
                                                            @endif
                                                        </select>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                {{ $cmsAccounts->appends(request()->query())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </div>
@endsection
@section('js')
    <script>
        @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{ session('message') }}'
        });
        @endif

        function redirect(event) {
            let res, url;
            let id = event.getAttribute('cms-account-id');
            switch (event.value) {
                case "edit":
                    url = "{{ route("cms-account.edit", "%id%") }}";
                    res = url.replace('%id%', id)
                    window.location.href = res;
                    break;
                case "destroy":
                    url = "{{ route("cms-account.destroy", "%id%") }}";
                    res = url.replace('%id%', id)
                    console.log(res);
                    confirmDelete(res);
                    break;
            }
        }
    </script>
@endsection



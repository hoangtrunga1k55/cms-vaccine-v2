@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Thêm tài khoản quản trị </h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="content">
            <div class="content-fluid">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">@if(isset($cmsAccount)) Sửa @else Tạo @endif tài khoản</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form
                        @if(isset($cmsAccount))
                        action="{{ route('cms-account.update', [$cmsAccount->id]) }}"
                        @else
                        action="{{ route('cms-account.store') }}"
                        @endif
                        method="post"
                        enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="card-body row">
                            <div class="col-6">
                                <div class="form-group ">
                                    <label for="full_name">Họ tên</label>
                                    <input type="text" class="form-control" name="full_name" id="full_name"
                                           placeholder="Nhập Họ và tên" required
                                           value="{{ old('full_name', $cmsAccount->full_name ?? null) }}">
                                    @error('full_name')
                                    <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                <div class="form-group ">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" name="email" id="email"
                                           placeholder="Nhập Email" required
                                           value="{{ old('email', $cmsAccount->email ?? null) }}">
                                    @error('email')
                                    <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                @if(!isset($cmsAccount))
                                    <div class="form-group">
                                        <label for="password">Mật khẩu</label>
                                        <input type="password" class="form-control" name="password" id="password"
                                               placeholder="Nhập mật khẩu" required>
                                        @error('password')
                                        <small class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                @endif
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Vai trò</label>
                                    <select class="form-control select2" id="roles" name="roles[]" multiple required>

                                        @if(isset($cmsAccount->roles) && $cmsAccount->roles->toArray()) != [])
                                        @foreach($roles as $role)
                                            <option
                                                value="{{ $role->id }}"
                                                {{ in_array($role->id, old('roles', $cmsAccount->role_ids)) ? "selected" : null }}>
                                                {{ $role->name}}
                                            </option>
                                        @endforeach
                                        @else
                                            @foreach($roles as $role)
                                                <option
                                                    {{ in_array($role->id, old('roles', [])) ? "selected" : null }}
                                                    value="{{ $role->id }}">{{ $role->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @error('roles')
                                    <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Trạng thái</label>
                                    <select class="form-control" id="status" name="status" required>

                                        @if(isset($cmsAccount->status))
                                            <option value="1" {{ $cmsAccount->status === "1" ? "selected" : null }}>
                                                Kích họat
                                            </option>
                                            <option value="0" {{ $cmsAccount->status === "0" ? "selected" : null }}>
                                                Hủy bỏ
                                            </option>
                                        @else
                                            <option value="1">Kích họat</option>
                                            <option value="0">Hủy bỏ</option>
                                        @endif
                                    </select>
                                    @error('status')
                                    <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                <label for="avatar">Hình ảnh</label>
                                <div class="input-group">
                                                           <span class="input-group-btn">
                                                             <a data-input="avatar" data-preview="holder" class="lfm btn btn-primary">
                                                               <i class="fa fa-picture-o"></i> Chọn ảnh
                                                             </a>
                                                           </span>
                                    <input id="avatar" class="form-control" type="text" name="avatar" value="{{ isset($cmsAccount->avatar) ? $cmsAccount->avatar : old('avatar')}}">
                                </div>
                                @error('avatar')
                                <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>

                            <div class="form-group col-3 image-avatar">
                                <img class="img-thumbnail"
                                     src="{{ $cmsAccount->avatar ?? null }}" alt="">
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('assets/admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <script src="{{ asset('assets/admin/plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
        $('.select2').select2()
    </script>
@endsection

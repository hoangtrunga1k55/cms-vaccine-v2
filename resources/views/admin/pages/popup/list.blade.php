@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1329.44px;">
        <section class="content-header">
            <section class="content">
                <div class="row">
                    <div class="col-12">
                        <h1>Danh Sách Popup</h1>
                        <br>
                        <div class="card">
                            <!-- /.card-header -->
                            <div class="card-body">
                                <form class="row">
                                    <div class="col-6 col-xl-3">
                                        <div class="form-group ">
                                            <label>Tên cấu hình</label>
                                            <input type="text" class="form-control" name="title" id="title"
                                                   placeholder="Nhập Tên popup" value="{{$request->title}}">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-success">Tìm kiếm
                                        </button>
                                        <button id="button_refresh" class="btn btn-info">Làm mới</button>
                                    </div>
                                </form>
                                <div class="row pt-5">
                                    <p>Có tất cả {{ $popups->total() }} danh mục thỏa mãn điều kiện tìm kiếm</p>
                                    <div class="col-sm-12">
                                        <table id="example2"
                                               class="table table-striped vertical"
                                               role="grid">
                                            <thead>
                                            <tr role="row">
                                                <th>#</th>
                                                <th>Tựa đề</th>
                                                <th>Thumbnail</th>
                                                <th>Danh mục</th>
                                                <th>Hành động</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($popups as $popup)
                                                <tr>
                                                    <td>
                                                        #{{ ($popups->currentPage()-1) * $popups->perPage() + $loop->iteration }}
                                                    </td>
                                                    <td>{{$popup->title ?? null}}</td>
                                                    <td><img height="85px" src="{{ $popup->image ?? null }}" alt="">
                                                    </td>
                                                    <td>@foreach($popup->positions ?? [] as $position)
                                                            {{ $position }}@if(!$loop->last), @endif
                                                        @endforeach</td>
                                                    <td>
                                                        <select class="form-control" onchange="redirect(this)"
                                                                popup_id="{{ $popup->id }}">
                                                            <option>Chọn hành động</option>
                                                            @if( \App\Helpers\PermissionsHelper::can('admin.popup.edit'))
                                                                <option
                                                                    value="edit">
                                                                    Sửa popup
                                                                </option>
                                                            @endif
                                                            @if( \App\Helpers\PermissionsHelper::can('admin.popup.destroy'))
                                                                <option
                                                                    value="destroy">
                                                                    Xóa popup
                                                                </option>
                                                            @endif
                                                        </select>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                            {{ $popups->appends(request()->query())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </div>
@endsection
@section('js')
    <script>
        @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{ session('message') }}'
        });
        @endif

        function redirect(event) {
            let res, url;
            let id = event.getAttribute('popup_id');
            switch (event.value) {
                case "edit":
                    url = "{{ route("popup.edit", "%id%") }}";
                    res = url.replace('%id%', id)
                    window.location.href = res;
                    break;
                case "destroy":
                    url = "{{ route("popup.destroy", "%id%") }}";
                    res = url.replace('%id%', id)
                    confirmDelete(res);
                    break;
            }
        }
    </script>
@endsection



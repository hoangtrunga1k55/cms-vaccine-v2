@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Popup</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">@if(isset($popup)) Sửa @else Tạo @endif Popup</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form @if(isset($popup))
                              action="{{ route('popup.update', [$popup->id]) }}"
                              @else
                              action="{{ route('popup.store') }}"
                              @endif
                              method="post" role="form"
                              onsubmit="submitForm(this); return false;"
                              enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="title">Tựa đề</label>
                                    <input type="text" class="form-control" name="title"
                                           value="{{ old('title', $popup->title ?? "")}}" id="title"
                                           placeholder="Nhập Tựa đề">
                                    <small class="text-danger rule" id="rule-title"></small>
                                </div>
                                <div class="form-group">
                                    <label for="content">Nội dung</label>
                                    <input type="text" class="form-control" name="content"
                                           value="{{ old('content', $popup->content ?? "")}}" id="content"
                                           placeholder="Nhập Nội dung">
                                    <small class="text-danger rule" id="rule-content"></small>
                                </div>
                                <div class="form-group">
                                    <label for="name">Vị trí</label>
                                    <div class="select2-pink">
                                        <select class="postion_mul form-control select2" name="positions[]"
                                                multiple="multiple">
                                            @foreach(config('define.POPUP_POSITION') as $item)
                                                <option
                                                    @if(in_array($item, old('positions', $popup->positions ?? [])))
                                                    selected
                                                    @endif
                                                    value="{{ $item }}">{{ $item }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <small class="text-danger rule" id="rule-positions"></small>
                                </div>
                                <div class="form-group">
                                    <label for="name">Bài viết</label>
                                    <select class="form-control" name="post_id" id="post_id">
                                        @foreach($news as $new)
                                            <option
                                                @if($popup->post_id ?? null == $new->id )))
                                                selected
                                                @endif
                                                value="{{ $new->_id }}">{{ $new->title }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <small class="text-danger rule" id="rule-post_id"></small>
                                </div>
                                <div class="form-group col-12">
                                    <label for="name">Hình Ảnh</label>
                                    <div class="input-group">
                                                           <span class="input-group-btn">
                                                             <a data-input="image" data-preview="holder"
                                                                class="lfm btn btn-primary">
                                                               <i class="fa fa-picture-o"></i> Choose
                                                             </a>
                                                           </span>
                                        <input id="image" class="form-control" type="text" name="image"
                                               value="{{ isset($popup->image) ? $popup->image : old('image')}}">
                                    </div>
                                </div>
                                <small class="text-danger rule" id="rule-image"></small>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-submit">@if(isset($vendor)) Sửa @else
                                        Tạo @endif</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script src="{{ asset('assets/admin/custom/ajax-submit.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('assets/admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <script src="{{ asset('assets/admin/plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
        $('.select2').select2()
        $("select.postion_mul ").select2({
            tags: true,
        })
    </script>
@endsection

@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1329.44px;">
        <section class="content-header">
            <section class="content">
                <div class="content-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h1>Danh Sách Danh Mục</h1>
                            <br>
                            <div class="card">
                                <div class="card-body">
                                    <form class="row">
                                        <div class="col-6 col-xl-3">
                                            <div class="form-group ">
                                                <label>Tên danh mục</label>
                                                <input type="text" class="form-control" name="name" id="name"
                                                       placeholder="Nhập tên" value="{{$request->name}}">
                                            </div>
                                        </div>
                                        <div class="col-6 col-xl-3">
                                            <div class="form-group">
                                                <label for="filter_status">Trạng thái</label>
                                                <select name="status" id="filter_status" class="form-control">
                                                    <option value="">Chọn trạng thái</option>
                                                    <option
                                                        value="1" {{($request->status == 1)?'selected':''}}>
                                                        Hiển thị
                                                    </option>
                                                    <option
                                                        value="0" {{($request->status == 0 && !is_null($request->status))?'selected':''}}>
                                                        Ẩn
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <button type="submit" class="btn btn-success">Tìm kiếm
                                            </button>
                                            <button id="button_refresh" class="btn btn-info">Làm mới</button>
                                        </div>
                                    </form>
                                    <div class="row pt-5">
                                        <p>Có tất cả {{ $categories->total() }} kết quả thỏa mãn điều kiện tìm kiếm</p>
                                        <div class="col-sm-12">
                                            <table id="example2"
                                                   class="table table-striped vertical"
                                                   role="grid">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Tên</th>
                                                    <th>Mô Tả</th>
                                                    <th>Danh Mục</th>
                                                    <th>Trạng Thái</th>
                                                    <th>Hành Động</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($categories as $category)
                                                    <tr class="even">
                                                        <td>
                                                            #{{ ($categories->currentPage()-1) * $categories->perPage() + $loop->iteration }}</td>
                                                        <td>{{$category->name ?? ""}}</td>
                                                        <td>{{$category->description ?? ""}}</td>
                                                        <td>
                                                            @foreach(explode(',',$category->parent_id) as $id_parent)
                                                                @foreach($categories as $cate_parent)
                                                                    @if($cate_parent->id == $id_parent)
                                                                        {{$cate_parent->name}}
                                                                    @endif
                                                                @endforeach
                                                            @endforeach
                                                        </td>
                                                        <td>
                                                            @if($category->status ==1)
                                                                <span class="badge badge-success">Hiển thị</span>
                                                            @else
                                                                <span class="badge badge-success">Ẩn</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <select class="form-control" onchange="redirect(this)"
                                                                    category-id="{{ $category->id }}">
                                                                <option>Chọn hành động</option>
                                                                @if( \App\Helpers\PermissionsHelper::can('admin.category.edit'))
                                                                    <option
                                                                        value="edit">
                                                                        Sửa danh mục
                                                                    </option>
                                                                @endif
                                                                @if( \App\Helpers\PermissionsHelper::can('admin.category.destroy'))
                                                                    <option
                                                                        value="destroy">
                                                                        Xóa danh mục
                                                                    </option>
                                                                @endif
                                                            </select>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    {{ $categories->appends(request()->query())->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </div>
@endsection
@section('js')
    <script>
        @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{ session('message') }}'
        });
        @endif

        function redirect(event) {
            let res, url;
            let id = event.getAttribute('category-id');
            switch (event.value) {
                case "edit":
                    url = "{{ route("category.edit", "%id%") }}";
                    res = url.replace('%id%', id)
                    window.location.href = res;
                    break;
                case "destroy":
                    url = "{{ route("category.destroy", "%id%") }}";
                    res = url.replace('%id%', id)
                    confirmDelete(res);
                    break;
            }
        }
    </script>
@endsection



@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1329.44px;">
        <section class="content-header">
            <section class="content">
                <div class="content-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h1>Danh sách Vaccine</h1>
                            <br>
                            <div class="card">
                                <div class="card-body">
                                    <form class="row" method="get" id="search">
                                        <div class="col-lg-3 col-6">
                                            <div class="form-group">
                                                <label for="status">Trạng thái</label>
                                                <select name="status" id="filter_status" class="form-control">
                                                    <option value="">Trạng thái</option>
                                                    <option
                                                        value="1" {{ ($request->status == 1) ? 'selected' : '' }}>
                                                        Hiển thị
                                                    </option>
                                                    <option
                                                        value="0" {{ ($request->status == 0) ? 'selected' : '' }}>
                                                        Ẩn
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <button type="submit" class="btn btn-success">Tìm kiếm
                                            </button>
                                            <button id="button_refresh" class="btn btn-info">Làm mới</button>
                                        </div>
                                    </form>
                                    <div class="row pt-5">
                                        <p>Có tất cả {{ $vaccines->total() }} kết quả thỏa mãn điều kiện tìm kiếm</p>
                                        <div class="col-sm-12">
                                            <table id="example2" class="table table-striped vertical"
                                                   role="grid">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Tên bệnh</th>
                                                    <th>Thời gian tạo</th>
                                                    <th>Thời gian cập nhật cuối</th>
                                                    <th>Hành Động</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($vaccines as $vaccine)
                                                    <tr class="even">
                                                        <td>{{ ($vaccines->currentPage()-1) * $vaccines->perPage() + $loop->iteration }}</td>
                                                        <td>{{$vaccine->name}}</td>
                                                        <td>{{$vaccine->created_at}}</td>
                                                        <td>{{$vaccine->updated_at}}</td>
                                                        <td>
                                                            <select class="form-control" onchange="redirect(this)"
                                                                    vaccine_id="{{ $vaccine->id }}">
                                                                <option>Chọn hành động</option>
                                                                @if( \App\Helpers\PermissionsHelper::can('admin.vaccine.edit'))
                                                                    <option
                                                                        value="edit">
                                                                        Sửa vaccine
                                                                    </option>
                                                                @endif
                                                                @if( \App\Helpers\PermissionsHelper::can('admin.vaccine.destroy'))
                                                                    <option
                                                                        value="destroy">
                                                                        Xóa vaccine
                                                                    </option>
                                                                @endif
                                                            </select>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    {{ $vaccines->appends(request()->query())->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </div>
@endsection
@section('js')
    <script>
        @if(session('success'))
        Toast.fire({
            icon: 'success',
            title: '{{ session('success') }}'
        });
        @endif

        function redirect(event) {
            let res, url;
            let id = event.getAttribute('vaccine_id');
            switch (event.value) {
                case "edit":
                    url = "{{ route("vaccine.edit", "%id%") }}";
                    res = url.replace('%id%', id)
                    window.location.href = res;
                    break;
                case "destroy":
                    url = "{{ route("vaccine.destroy", "%id%") }}";
                    res = url.replace('%id%', id)
                    console.log(res)
                    confirmDelete(res);
                    break;
            }
        }
    </script>
@endsection



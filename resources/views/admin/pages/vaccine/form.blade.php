@extends('admin.layouts.master')

@section('title')
    {{(isset($vaccine) ? 'Sửa' : 'Thêm' ) }} vắc xin
@endsection
@section('css')
    <link href="{{asset('assets/admin/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('assets/admin/plugins/iCheck/all.css')}}" rel="stylesheet" media="all">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    <style>
        .select2-container {
            width: 100% !important;
        }
        .select-user{
            margin:10px 0px;
        }
        .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__arrow{
            height: 34px;
        }
    </style>
    <script>
        let data = {!! $pathogens !!};
        let dataFromServerSide = JSON.parse(JSON.stringify(data));
    </script>
@endsection
@section('content')
    <style>
        .ck.ck-editor__editable_inline {
            border: 1px solid #C4C4C4!important;
        }
    </style>
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{(isset($vaccine) ? 'Sửa' : 'Thêm' ) }} vaccine</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="content">
            <div class="content-fluid">
                <div class="row">
                    <div id="app" style="width: 100%">
                        <add-vaccine></add-vaccine>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset('assets/admin/plugins/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/iCheck/icheck.min.js')}}"></script>
    <script src="//gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
@endsection

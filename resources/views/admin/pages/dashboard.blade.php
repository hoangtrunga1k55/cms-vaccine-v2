@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 326px">
        <section class="content">
            <div class="content-fluid">
                <h1>Bảng điều khiển</h1>
                <div class="row">
                    <div class="col-lg-4 col-6">
                        <!-- small box -->
                        <div class="small-box bg-info">
                            <div class="inner">
                                <h3>{{ $countUser }}</h3>
                                <p>Tổng số người dùng</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                            <a href="{{ route('cms-account.index') }}" class="small-box-footer">Xem chi tiết <i
                                    class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-4 col-6">
                        <!-- small box -->
                        <div class="small-box bg-success">
                            <div class="inner">
                                <h3>{{$countNew}}</h3>

                                <p>Tổng số bài viết</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            <a href="{{ route('news.index') }}" class="small-box-footer">Xem chi tiết <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-4 col-6">
                        <!-- small box -->
                        <div class="small-box bg-warning">
                            <div class="inner">
                                <h3>{{ $countVaccine }}</h3>

                                <p>Tổng số vaccines</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-add"></i>
                            </div>
                            <a href="{{ route('vaccine.index') }}" class="small-box-footer">Xem chi tiết <i
                                    class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>

                    <div class="col-lg-6 col-12">
                        <!-- AREA CHART -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Thông báo đã gửi</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="chart">
                                    <div class="chartjs-size-monitor">
                                        <div class="chartjs-size-monitor-expand">
                                            <div class=""></div>
                                        </div>
                                        <div class="chartjs-size-monitor-shrink">
                                            <div class=""></div>
                                        </div>
                                    </div>
                                    <canvas id="notify_chart"
                                            style="min-height: 250px" class="chartjs-render-monitor"></canvas>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                    </div>
                    <div class="col-lg-6 col-12">
                        <!-- AREA CHART -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Bài viết đã tạo</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="chart">
                                    <div class="chartjs-size-monitor">
                                        <div class="chartjs-size-monitor-expand">
                                            <div class=""></div>
                                        </div>
                                        <div class="chartjs-size-monitor-shrink">
                                            <div class=""></div>
                                        </div>
                                    </div>
                                    <canvas id="articles_chart"
                                            style="min-height: 250px" class="chartjs-render-monitor"></canvas>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <div class="col-lg-6 col-12">
                        <!-- AREA CHART -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">NRU (Số người dùng mới)</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="chart">
                                    <div class="chartjs-size-monitor">
                                        <div class="chartjs-size-monitor-expand">
                                            <div class=""></div>
                                        </div>
                                        <div class="chartjs-size-monitor-shrink">
                                            <div class=""></div>
                                        </div>
                                    </div>
                                    <canvas id="nru_chart"
                                            style="min-height: 250px" class="chartjs-render-monitor"></canvas>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('js')
    <script src="{{ asset('assets/admin/plugins/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/chart.js/Chart.min.css') }}"></script>
    <script>
        let nru_chart_element = $('#nru_chart');
        let articles_chart_element = $('#articles_chart');
        let notify_chart_element = $('#notify_chart');

        let nru_chart_data = {
            labels: [
                @foreach($nru as $date=>$item) "{{$date}}" @if(!$loop->last) , @endif @endforeach
            ],
            datasets: [{
                label: 'Số người dùng mới',
                data: [
                    @foreach($nru as $date=>$item) "{{ $item->count() }}" @if(!$loop->last) , @endif @endforeach
                ],
                fill: false,
                borderColor: 'rgb(75, 192, 192)',
                tension: 0.1
            }]
        };

        let article_chart_data = {
            labels: [
                @foreach($articles as $date=>$item) "{{$date}}" @if(!$loop->last) , @endif @endforeach
            ],
            datasets: [{
                label: 'Bài viết đã tạo',
                data: [
                    @foreach($articles as $date=>$item) "{{ $item->count() }}" @if(!$loop->last) , @endif @endforeach
                ],
                fill: false,
                borderColor: 'rgb(75, 192, 192)',
                tension: 0.1
            }]
        };

        let notify_chart_data = {
            labels: [
                @foreach($notify as $date=>$item) "{{$date}}" @if(!$loop->last) , @endif @endforeach
            ],
            datasets: [{
                label: 'Thông báo đã gửi',
                data: [
                    @foreach($notify as $date=>$item) "{{ $item->count() }}" @if(!$loop->last) , @endif @endforeach
                ],
                fill: false,
                borderColor: 'rgb(75, 192, 192)',
                tension: 0.1
            }]
        };
        const nru_chart = new Chart(nru_chart_element, {
            type: "line",
            data: nru_chart_data
        })
        const notify_chart = new Chart(notify_chart_element, {
            type: "line",
            data: notify_chart_data
        })
        const article_chart = new Chart(articles_chart_element, {
            type: "line",
            data: article_chart_data
        })
    </script>
@endsection

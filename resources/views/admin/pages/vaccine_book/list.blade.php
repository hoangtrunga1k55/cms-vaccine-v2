@extends('admin.layouts.master')
@section('content')
<div class="content-wrapper" style="min-height: 1329.44px;">
    <section class="content-header">
        <section class="content">
            <div class="content-fluid">
                <div class="row">
                    <div class="col-12">
                        <h1>Danh Sách Sổ Tiêm</h1>
                        <br>
                        <div class="card">
                            <!-- /.card-header -->
                            <div class="card-body">
                                <form class="row">
                                    <div class="col-12 col-xl-3">
                                        <div class="form-group ">
                                            <label>Tên khách hàng</label>
                                            <input type="text" class="form-control" name="user_full_name" id="user_full_name"
                                                placeholder="Nhập tên" value="{{$request->user_full_name}}">
                                        </div>
                                    </div>
                                    <div class="col-6 col-xl-3">
                                        <div class="form-group ">
                                            <label>Năm sinh từ</label>
                                            <input type="number" min="1900" max="2099" step="1" class="form-control" name="user_full_name" id="user_full_name"
                                                placeholder="Nhập năm sinh" value="{{$request->dob_from}}">
                                        </div>
                                    </div>
                                    <div class="col-6 col-xl-3">
                                        <div class="form-group ">
                                            <label>Năm sinh đến</label>
                                            <input type="number" min="1900" max="2099" step="1" class="form-control" name="dob_to" id="dob_to"
                                                   placeholder="Nhập năm sinh đến" value="{{$request->dob_to}}">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-success">Tìm kiếm
                                        </button>
                                        <button id="button_refresh" class="btn btn-info">Làm mới</button>
                                    </div>
                                </form>
                                <div class="row pt-5">
                                    <p>Có tất cả {{ $vaccine_books->total() }} kết quả thỏa mãn điều kiện tìm kiếm</p>
                                    <div class="col-sm-12">
                                        <table id="example2" class="table table-striped vertical" role="grid">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Tên</th>
                                                    <th>Giới tính</th>
                                                    <th>Chiều cao</th>
                                                    <th>Cân nặng</th>
                                                    <th>Người tạo</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($vaccine_books as $key => $row)
                                                <tr class="even">
                                                    <td>#{{ ($vaccine_books->currentPage()-1) * $vaccine_books->perPage() + $loop->iteration }}
                                                    </td>

                                                    <td>{{!empty($row->user_full_name) ? $row->user_full_name : "-"}}
                                                    </td>
                                                    <td>{{!empty($row->gender) ? $row->getGender() : "-"}}</td>
                                                    <td>{{!empty($row->height) ? $row->height : "-"}}</td>
                                                    <td>{{!empty($row->weight) ? $row->weight : "-"}}</td>
                                                    <td>{{!empty($row->user_info->name)?$row->user_info->name : "-"}}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                {{ $vaccine_books->appends(request()->query())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
</div>
@endsection
@section('js')
<script>
    @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{ session('message') }}'
        });
        @endif
</script>
@endsection

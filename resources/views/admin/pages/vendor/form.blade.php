@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Nhà Cung Cấp</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="content">
            <div class="content-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">@if(isset($vendor)) Sửa @else Tạo @endif Nhà cung cấp</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form @if(isset($vendor))
                                  action="{{ route('vendor.update', [$vendor->id]) }}"
                                  @else
                                  action="{{ route('vendor.store') }}"
                                  @endif
                                  method="post" role="form"
                                  onsubmit="submitForm(this); return false;"
                                  enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="title">Tên nhà cung cấp</label>
                                        <input type="text" class="form-control" name="title"
                                               value="{{ old('title', $vendor->title ?? "")}}" id="title"
                                               placeholder="Nhập Tên nhà cung cấp">
                                        <small class="text-danger rule" id="rule-title"></small>
                                    </div>
                                    <div class="form-group">
                                        <label for="decription">Mô tả</label>
                                        <input type="text" class="form-control" name="description"
                                               value="{{ old('description', $vendor->description ?? "")}}"
                                               id="description"
                                               placeholder="Nhập Mô Tả">
                                        <small class="text-danger rule" id="rule-description"></small>
                                    </div>

                                    <div class="form-group">
                                        <label>Trạng Thái</label>
                                        <select class="form-control" id="status" name="status">
                                            @if(isset($vendor->status))
                                                <option
                                                    value="1" {{ old('status', $vendor->status) === "1" ? "selected" : null }}>
                                                    Hiển thị
                                                </option>
                                                <option
                                                    value="0" {{ old('status', $vendor->status) === "0" ? "selected" : null }}>
                                                    Ẩn
                                                </option>
                                            @else
                                                <option value="1" {{ old('status') === "1" ? "selected" : null }}>Hiển
                                                    thị
                                                </option>
                                                <option value="0" {{ old('status') === "0" ? "selected" : null }}>Ẩn
                                                </option>
                                            @endif
                                        </select>
                                        <small class="text-danger rule" id="rule-status"></small>
                                    </div>
                                    <div class="form-group">
                                        <label for="image">Hình Ảnh</label>
                                        <div class="input-group">
                                                           <span class="input-group-btn">
                                                             <a data-input="image" data-preview="holder" class="lfm btn btn-primary">
                                                               <i class="fa fa-picture-o"></i> Choose
                                                             </a>
                                                           </span>
                                            <input id="image" class="form-control" type="text" name="image" value="{{ old('image', $vendor->image ?? "") }}">
                                        </div>
                                        <small class="text-danger rule" id="rule-image"></small>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-submit">@if(isset($vendor)) Sửa @else Tạo @endif</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script src="{{ asset('assets/admin/custom/ajax-submit.js') }}"></script>
@endsection

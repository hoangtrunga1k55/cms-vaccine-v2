@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1329.44px;">
        <section class="content-header">
            <section class="content">
                <div class="content-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h1>Danh Sách Nhà Cung Cấp</h1>
                            <br>
                            <div class="card">
                                <div class="card-body">
                                    <form class="row" method="get" id="search">
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-3 col-6">
                                                    <!-- text input -->
                                                    <div class="form-group">
                                                        <label for="status">Trạng thái</label>
                                                        <select name="status" id="filter_status" class="form-control">
                                                            <option value="">Trạng thái</option>
                                                            <option
                                                                value="1" {{ ($request->status == 1) ? 'selected' : '' }}>
                                                                Hiển thị
                                                            </option>
                                                            <option
                                                                value="0" {{ ($request->status == 0) ? 'selected' : '' }}>
                                                                Ẩn
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <button type="submit" form="search" class="btn btn-success">Tìm kiếm
                                                    </button>
                                                    <button id="button_refresh" class="btn btn-info">Làm mới</button>
                                                </div>
                                            </div>
                                            <!-- /.card-footer -->
                                        </div>

                                        <!-- /.card-body -->
                                    </form>
                                    <div class="row pt-5">
                                        <p>Có tất cả {{ $vendors->total() }} kết quả thỏa mãn điều kiện tìm kiếm</p>
                                        <div class="col-12">
                                            <table id="example2" class="table table-striped vertical"
                                                   role="grid">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Tiêu Đề</th>
                                                    <th>Hình Ảnh</th>
                                                    <th>Mô Tả</th>
                                                    <th>Trạng Thái</th>
                                                    <th>Hành Động</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($vendors as $vendor)
                                                    <tr class="even">
                                                        <td>{{ ($vendors->currentPage()-1) * $vendors->perPage() + $loop->iteration }}</td>
                                                        <td>{{$vendor->title}}</td>
                                                        <td><img
                                                                src="{{ \App\Helpers\ImageHelper::getImage($vendor->image) }}"
                                                                height="50px" width="50px" alt=""></td>
                                                        <td>{{$vendor->description}}</td>
                                                        @if($vendor->status ==1)
                                                            <td>Hiển thị</td>
                                                        @else
                                                            <td>Ẩn</td>
                                                        @endif
                                                        <td>
                                                            <select class="form-control" onchange="redirect(this)"
                                                                    vendor-id="{{ $vendor->id }}">
                                                                <option>Chọn hành động</option>
                                                                @if( \App\Helpers\PermissionsHelper::can('admin.vendor.edit'))
                                                                    <option
                                                                        value="edit">
                                                                        Sửa nhà cung cấp
                                                                    </option>
                                                                @endif
                                                                @if( \App\Helpers\PermissionsHelper::can('admin.vendor.destroy'))
                                                                    <option
                                                                        value="destroy">
                                                                        Xóa nhà cung cấp
                                                                    </option>
                                                                @endif
                                                            </select>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                {{ $vendors->appends(request()->query())->links() }}
                            </div>
                        </div>
                    </div>
                </div>

            </section>
        </section>
    </div>
@endsection
@section('js')
    <script>
        @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{ session('message') }}'
        });
        @endif

        function redirect(event) {
            let res, url;
            let id = event.getAttribute('vendor-id');
            switch (event.value) {
                case "edit":
                    url = "{{ route("vendor.edit", "%id%") }}";
                    res = url.replace('%id%', id)
                    window.location.href = res;
                    break;
                case "destroy":
                    url = "{{ route("vendor.destroy", "%id%") }}";
                    res = url.replace('%id%', id)
                    confirmDelete(res);
                    break;
            }
        }
    </script>
@endsection



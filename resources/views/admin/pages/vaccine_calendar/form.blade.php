@extends('admin.layouts.master')

@section('title')
    Quản lý lịch tiêm
@endsection
@section('css')
    <link href="{{asset('assets/admin/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}"
          rel="stylesheet" media="all">
    <link href="{{asset('assets/admin/plugins/iCheck/all.css')}}" rel="stylesheet" media="all">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet"/>
    <style>
        .select2-container {
            width: 100% !important;
        }

        .select-user {
            margin: 10px 0px;
        }

        .select2-container--default .select2-selection--single,
        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 34px;
        }

        .div-avatar {
            width: 100%;
            padding-top: 100%;
            background-color: #F1F1F1;
            border-radius: 5px;
            border: 1px solid #F1F1F1;
            position: relative;
        }

        .div-avatar:hover {
            border: 1px dotted #D0D0D0;
            cursor: pointer;
        }

        .show-avatar {
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            display: flex;
            align-items: center;
            font-size: 50px;
            justify-content: center;
            color: #C0C0C0;
        }
    </style>
    <script>
        let dataFromServerSide = {
            vaccines: {!! json_encode($vaccines) !!},
            calendar: {!! json_encode($calendar) !!},
        };
    </script>
@endsection
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Lịch tiêm</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <div class="content">
            <div class="row" id="app">
                <div class="card col-12">
                    <div class="card-body">
                        <add-vaccine-calendar></add-vaccine-calendar>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset('assets/admin/plugins/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script
        src="{{asset('assets/admin/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js')}}">
    </script>
    <script src="{{asset('assets/admin/plugins/iCheck/icheck.min.js')}}"></script>
    <script src="//gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>

    <script>
    </script>
@endsection

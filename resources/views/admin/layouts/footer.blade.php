<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
        <b>Version</b> 3.1.0-rc
    </div>
    <strong>Copyright &copy;<a href="https://adminlte.io">AMV Group</a>.</strong> All rights reserved.
</footer>

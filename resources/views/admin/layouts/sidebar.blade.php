<!-- Main Sidebar Container -->
<?php
use Illuminate\Support\Facades\Config;
$listes = Config::get('menu');
?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('dashboard')}}" class="brand-link">
        <img src="{{ asset('assets/admin/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo"
             class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('assets/admin/dist/img/user2-160x160.jpg')}} " class="img-circle elevation-2"
                     alt="User Image">
            </div>
            <div class="info">
                <a href="{{route('cms-account.index')}}" class="d-block">{{\Illuminate\Support\Facades\Auth::user()->name??'Admin'}}</  a>
            </div>
        </div>

        <!-- SidebarSearch Form -->
{{--        <div class="form-inline">--}}
{{--            <div class="input-group" data-widget="sidebar-search">--}}
{{--                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">--}}
{{--                <div class="input-group-append">--}}
{{--                    <button class="btn btn-sidebar">--}}
{{--                        <i class="fas fa-search fa-fw"></i>--}}
{{--                    </button>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                @foreach($listes as $list)
                    @if(\App\Helpers\PermissionsHelper::can($list['route']))
                        <li class="nav-item {{ in_array(\Request::route()->getPrefix(), $list['check_active_prefix']) ? "menu-is-opening menu-open" : " " }}">
                            <a href="{{ route($list['route']) }}"
                               class="nav-link {{ \Request::route()->getName()==$list['route'] ? "active" : " " }}">
                                <i class="nav-icon {{ $list['icon_class'] }}" aria-hidden="true"></i>
                                <p>
                                    {{ $list['title'] }}
                                    @if(!empty($list['sub_menus']))
                                        <i class="right fas fa-angle-left"></i>
                                    @endif
                                </p>
                            </a>
                            @if(!empty($list['sub_menus']))
                                <ul class="nav nav-treeview" style="display: none;">
                                    @if(!empty($list['sub_menus']))
                                        @foreach($list['sub_menus'] as $detail)
                                            @if(\App\Helpers\PermissionsHelper::can($detail['route']))
                                            <li class="nav-item" style="padding-left: 32px">
                                                <a href="{{route($detail['route'])}}"
                                                   class="nav-link {{ \Request::route()->getName()==$detail['route'] ? "active" : " " }}">
                                                    <p>{{$detail['title']}}</p>
                                                </a>
                                            </li>
                                            @endif
                                        @endforeach
                                    @endif
                                </ul>
                            @endif
                        </li>
                    @endif
                @endforeach
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>


window.Vue = require('vue');
import Anted from 'ant-design-vue/es';
Vue.use(Anted);

import helpers from './helpers';
Vue.mixin({
    methods: helpers
})

import CKEditor from 'ckeditor4-vue';
Vue.use(CKEditor);

Vue.component('add-vaccine', require('./screens/Vaccine/add.vue').default);
Vue.component('add-vaccine-calendar', require('./screens/VaccineCalendar/add.vue').default);
Vue.component('add-vaccine-note', require('./screens/VaccineNote/add.vue').default);
const app = new Vue({
    el: '#app'
});

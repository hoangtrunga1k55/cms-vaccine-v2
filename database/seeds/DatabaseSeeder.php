<?php

use App\Repositories\Contracts\CmsAccountRepositoryInterface;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    protected $cmsAccountRepository;

    public function __construct(CmsAccountRepositoryInterface $cmsAccountRepository)
    {
        $this->cmsAccountRepository = $cmsAccountRepository;
    }
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->cmsAccountRepository->create([
            'email' => 'admin@gmail.com',
            'password' => Hash::make('Admin@11##'),
            'status' => '1',
            'role' => 'SA',
            'avatar' => ''
        ]);
    }
}

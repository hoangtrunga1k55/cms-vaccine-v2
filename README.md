How to run
1. docker-compose build
2. docker-compose up
3. enter link: http://127.0.0.1:8080

How to run artisan 
-Ex: 
 docker-compose exec app php artisan make:controller TestController

how to run file manager 1.8
1. Cài qua compose
composer require unisharp/laravel-filemanager:~1.8
2. publish config publish và assest
php artisan vendor:publish --tag=lfm_config
php artisan vendor:publish --tag=lfm_public
3. Xóa cache
php artisan route:clear
php artisan config:clear
4. Chỉnh lại APP_URL trong file .env đúng đường dẫn web của bạn, cấp quyền ghi cho thư mục file & ảnh được cấu hình trong file config/lfm.php
Cấu hình package
File cấu hình của Laravel-filemanager được lưu trong config/lfm.php:

-Với Routes:

use_package_routes: Sử dụng routes của package, mặc định là true, nếu false bạn phải định nghĩa lại tất cả các routes của package .
middlewares: Các routes mặc định sẽ phải đi qua các middleware được định nghĩa ở đây.
url_prefix: Định nghĩa tiền tố url.
allow_multi_user: Cho phép tạo lưu file & ảnh riêng cho mỗi user
allow_share_folder: Tạo thư mục chia sẻ.
-Thư mục lưu trữ của package:

base_directory: Tùy chỉnh thư mục lưu (public, resource, storage ...).
-Giao diện hiển thị:

images_startup_view: Kiểm hiển thị giao diện ảnh: 'grid', 'list'.
files_startup_view: Kiểm hiển thị giao diện file: 'grid', 'list'.
-Upload / Validation:

rename_file: nếu true, file và ảnh tải nên sẽ được đổi tên.
should_validate_size: file và ảnh tải nên sẽ được verify size.
max_image_size: size tối đa cho ảnh.
max_file_size: size tối đa cho file.
should_validate_mime: file và ảnh tải nên sẽ được verify kiểu file
valid_image_mimetypes: Khai báo các kiểu file được phép tải

5. config trong js
<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
<script type="text/javascript">
    $('.lfm').filemanager('image');
</script>
6. sửa lỗi
- thêm helper: composer require laravel/helpers
- Thêm publish view: php artisan vendor:publish --tag=lfm_view --force

//hướng dẫn cài laravel debugbar
1. php artisan vendor:publish --provider="Barryvdh\Debugbar\ServiceProvider"
2. sửa config trong app
+ APP_DEBUG=TRUE 
+DEBUGBAR_ENABLED=true
+ mongodb add 
  app\Providers\AppServiceProvicder.php
  function boot()
  {
  \DB::connection('mongodb')->enableQueryLog();
  }
3. xóa cache + restart server

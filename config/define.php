<?php
    return [
        'CATEGORY' => [
            'TYPE_VACCINE' => 1,
            'TYPE_NEWS' => 2
        ],
        'POPUP_POSITION' => ['HOME', 'DETAIL', 'CATEGORY']
    ];


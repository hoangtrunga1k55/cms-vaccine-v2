<?php
return [
    "Dashboard" => [
        'Xem Dashboard' => [
            'dashboard',
        ],
    ],
    "Quản lý tài khoản" => [
        'Quản lý tài khoản cms' => [
            'cms-account.index',
            'cms-account.create',
            'cms-account.store',
            'cms-account.edit',
            'cms-account.update',
            'cms-account.destroy',
        ]
    ],
    "Quản lý cấu hình" => [
        'Xem cấu hình' => [
            'config.index',
        ],
        'Thêm cấu hình' => [
            'config.create',
            'config.store',
        ],
        'Sửa cấu hình' => [
            'config.edit',
            'config.update',
        ],
        'Xóa cấu hình' => [
            'config.destroy',
        ],
    ],
    "Quản lý quyền" => [
        'Xem quyền' => [
            'permission.index',
        ],
        'Thêm quyền' => [
            'permission.create',
            'permission.store',
        ],
        'Sửa quyền' => [
            'permission.edit',
            'permission.update',
        ],
        'Xóa quyền' => [
            'permission.destroy',
        ],
    ],
    "Quản lý nhóm quyền" => [
        'Xem nhóm quyền' => [
            'role.index',
        ],
        'Thêm nhóm quyền' => [
            'role.create',
            'role.store',
        ],
        'Sửa nhóm quyền' => [
            'role.edit',
            'role.update',
        ],
        'Xóa nhóm quyền' => [
            'role.destroy',
        ],
        'Khởi tạo nhóm quyền' => [
            'role.destroy',
        ],
    ],
];

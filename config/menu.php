<?php
return [
    [
        'title' => 'Bảng Điều Khiển',
        'route' => 'dashboard',
        'permission' => '/',
        'check_active_prefix' => [''],
        'icon_class' => 'fas fa-tachometer-alt',
        'sub_menus' => []
    ],
    [
        'title' => 'Danh mục',
        'route' => "category.index",
        'check_active_prefix' => ['/category'],
        'icon_class' => 'fa fa-book',
        'sub_menus' => [
            [
                'title' => 'Danh sách danh mục',
                'route' => "category.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Tạo danh mục',
                'route' => "category.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Nhà cung cấp',
        'route' => "vendor.index",
        'check_active_prefix' => ['/vendors'],
        'icon_class' => 'fas fa-store',
        'sub_menus' => [
            [
                'title' => 'Danh sách nhà cung cấp',
                'route' => "vendor.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Tạo nhà cung cấp',
                'route' => "vendor.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Bài viết',
        'route' => "news.index",
        'check_active_prefix' => ['/news'],
        'icon_class' => 'fa fa-file-text-o',
        'sub_menus' => [
            [
                'title' => 'Danh sách bài viết',
                'route' => "news.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Tạo bài viết',
                'route' => "news.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Bệnh lý',
        'route' => "pathogen.index",
        'check_active_prefix' => ['/pathogen'],
        'icon_class' => 'fas fa-bacteria',
        'sub_menus' => [
            [
                'title' => 'Danh sách bệnh lý',
                'route' => "pathogen.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Tạo bệnh lý',
                'route' => "pathogen.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Phòng tiêm',
        'route' => "medicine-location.index",
        'check_active_prefix' => ['/medicine-location'],
        'icon_class' => 'far fa-hospital',
        'sub_menus' => [
            [
                'title' => 'Danh sách phòng tiêm',
                'route' => "medicine-location.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Tạo phòng tiêm',
                'route' => "medicine-location.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Vaccine',
        'route' => "vaccine.index",
        'check_active_prefix' => ['/vaccine'],
        'icon_class' => 'fas fa-syringe',
        'sub_menus' => [
            [
                'title' => 'Danh sách Vaccine',
                'route' => "vaccine.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Tạo Vaccine',
                'route' => "vaccine.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Đơn đặt tiêm',
        'route' => "order.index",
        'check_active_prefix' => ['/order/'],
        'icon_class' => 'fas fa-money-check-alt',
        'sub_menus' => []
    ],
    [
        'title' => 'Lịch tiêm',
        'route' => "vaccine_calendar",
        'check_active_prefix' => ['/vaccine_calendar'],
        'icon_class' => 'fa fa-calendar',
        'sub_menus' => []
    ],
    [
        'title' => 'Sổ tiêm',
        'route' => "vaccine_book",
        'check_active_prefix' => ['/vaccine_book'],
        'icon_class' => 'fas fa-book-medical',
        'sub_menus' => []
    ],
    [
        'title' => 'Quản lý popup',
        'route' => "popup.index",
        'check_active_prefix' => ['/popup'],
        'icon_class' => 'fa fa-plus-circle',
        'sub_menus' => [
            [
                'title' => 'Danh sách popup',
                'route' => "popup.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Tạo popup',
                'route' => "popup.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Quản lý thông báo',
        'route' => "notification.index",
        'check_active_prefix' => ['/notification'],
        'icon_class' => 'fa fa-bullhorn',
        'sub_menus' => [
            [
                'title' => 'Danh sách thông báo',
                'route' => "notification.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Tạo thông báo',
                'route' => "notification.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Quản lý ghi chú',
        'route' => "vaccine_note",
        'check_active_prefix' => ['/vaccine_note'],
        'icon_class' => 'fa fa-sticky-note',
        'sub_menus' => [
            [
                'title' => 'Danh sách ghi chú',
                'route' => "vaccine_note",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Quản lý thành viên',
        'route' => "users.index",
        'check_active_prefix' => ['/users'],
        'icon_class' => 'fa fa-address-book-o',
        'sub_menus' => [
            [
                'title' => 'Danh sách thành viên',
                'route' => "users.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Cấu Hình',
        'route' => "config.index",
        'check_active_prefix' => ['/config'],
        'icon_class' => 'fa fa-cogs',
        'sub_menus' => [
            [
                'title' => 'Danh Sách',
                'route' => "config.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Tạo Cấu Hình',
                'route' => "config.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Người Dùng',
        'route' => "cms-account.index",
        'check_active_prefix' => ['/cms-accounts'],
        'icon_class' => 'fa fa-user',
        'sub_menus' => [
            [
                'title' => 'Danh Sách',
                'route' => "cms-account.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Thêm Người Dùng',
                'route' => "cms-account.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Nhóm quyền',
        'route' => "permission.index",
        'check_active_prefix' => ['/permissions', '/roles'],
        'icon_class' => 'fa fa-address-card',
        'sub_menus' => [
            [
                'title' => 'Danh sách quyền',
                'route' => "permission.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Thêm quyền',
                'route' => "permission.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ],
            [
                'title' => 'Danh sách nhóm quyền',
                'route' => "role.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Thêm nhóm quyền',
                'route' => "role.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Đăng xuất',
        'route' => 'logout',
        'check_active_prefix' => ['/logout'],
        'icon_class' => 'fa fa-sign-out',
        'sub_menus' => []
    ],
];

<?php

return [
    [
        'title' => 'Dashboard',
        'route' => 'cp-admin',
        'permission' => '/',
        'check_active_prefix' => '',
        'icon_class' => 'fa fa-user',
        'sub_menus' => []
    ],
    [
        'title' => 'Category',
        'permission' => '',
        'route' => "cp-admin.category.list",
        'check_active_prefix' => 'cp-admin',
        'icon_class' => 'fa fa-edit',
        'sub_menus' =>
            [
                [
                    'title' => 'List Category',
                    'permission' => '',
                    'route' => "cp-admin.category.list",
                    'check_active_prefix' => 'cp-admin',
                    'icon_class' => 'fa fa-edit',
                    'sub_menus' => []
                ],
                [
                    'title' => 'Add Category',
                    'permission' => '',
                    'route' => "cp-admin.category.add",
                    'check_active_prefix' => 'cp-admin',
                    'icon_class' => 'fa fa-edit',
                    'sub_menus' => [],
                ]
            ]
    ],
    [
        'title' => 'News',
        'permission' => '',
        'route' => "cp-admin.new.list",
        'check_active_prefix' => 'cp-admin',
        'icon_class' => 'fa fa-edit',
        'sub_menus' => [
            [
                'title' => 'List New',
                'permission' => '',
                'route' => "cp-admin.new.list",
                'check_active_prefix' => 'cp-admin',
                'icon_class' => 'fa fa-edit',
                'sub_menus' => []
            ],
            [
                'title' => 'Add New',
                'permission' => '',
                'route' => "cp-admin.new.add",
                'check_active_prefix' => 'cp-admin',
                'icon_class' => 'fa fa-edit',
                'sub_menus' => []
            ]
        ]
    ]
];


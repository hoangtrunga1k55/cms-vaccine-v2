<?php
return [
    'cp-admin.category.store' => [
        'title' => 'Thêm Danh Mục',
        'extra_name_params' => ['title'],
        'insert_id' => 'id',
        'method' => 'post',
    ],
    'cp-admin.category.update' => [
        'title' => 'Sửa Danh Mục',
        'extra_name_params' => ['title'],
        'insert_id' => 'id',
        'method' => 'post',
    ],
    'cp-admin.category.delete' => [
        'title' => 'Xóa Danh Mục',
        'extra_name_params' => ['title'],
        'insert_id' => 'id',
        'method' => 'get',
    ],
    'cp-admin.new.store' => [
        'title' => 'Thêm Bài Viết',
        'extra_name_params' => ['title'],
        'insert_id' => 'id',
        'method' => 'post',
    ],
    'cp-admin.new.update' => [
        'title' => 'Sửa Danh Mục',
        'extra_name_params' => ['title'],
        'insert_id' => 'id',
        'method' => 'post',
    ],
    'cp-admin.new.delete' => [
        'title' => 'Xóa Danh Mục',
        'extra_name_params' => ['title'],
        'insert_id' => 'id',
        'method' => 'get',
    ],
    'cp-admin.config.store' => [
    'title' => 'Thêm Cấu Hình',
    'extra_name_params' => ['title'],
    'insert_id' => 'id',
    'method' => 'post',
],
    'cp-admin.config.update' => [
    'title' => 'Sửa Cấu Hình',
    'extra_name_params' => ['title'],
    'insert_id' => 'id',
    'method' => 'post',
],
    'cp-admin.config.delete' => [
    'title' => 'Xóa Cấu Hình',
    'extra_name_params' => ['title'],
    'insert_id' => 'id',
    'method' => 'get',
],
    'admin.cms-account.store' => [
        'title' => 'Thêm Tài Khoản',
        'extra_name_params' => ['title'],
        'insert_id' => 'id',
        'method' => 'post',
    ],
    'admin.cms-account.update' => [
        'title' => 'Sửa Tài Khoản',
        'extra_name_params' => ['title'],
        'insert_id' => 'id',
        'method' => 'post',
    ],
    'admin.cms-account.destroy' => [
        'title' => 'Xóa Tài Khoản',
        'extra_name_params' => ['title'],
        'insert_id' => 'id',
        'method' => 'get',
    ]

];

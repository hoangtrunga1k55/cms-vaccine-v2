<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('upload-image', 'Admin\FileController@uploadImage');
Route::post('upload-media', 'Admin\FileController@uploadMedia');
Route::prefix('vaccine')->group(function () {
    Route::post('/add', 'Admin\VaccineController@store')->name('vaccine.store');
});
Route::prefix('vaccine_calendar')->group(function () {
    Route::post('/save', 'Admin\VaccineCalendarController@save')->name('vaccine_calendar.save');
});
Route::prefix('vaccine_note')->group(function () {
    Route::post('/save', 'Admin\VaccineNoteController@save')->name('vaccine_note.save');
});

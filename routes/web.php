<?php

use App\Helpers\PushNotifyHelper;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CmsAccountController;
use App\Http\Controllers\Admin\ConfigController;
use App\Http\Controllers\Admin\MedicineLocationController;
use App\Http\Controllers\Admin\NewController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\PathogenController;
use App\Http\Controllers\Admin\NotificationController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\PopupController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\VaccineCalendarController;
use App\Http\Controllers\Admin\VaccineBookController;
use App\Http\Controllers\Admin\VaccineController;
use App\Http\Controllers\Admin\VendorController;
use \App\Http\Controllers\Admin\VaccineNoteController;
use \App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'namespace' => 'Auth'
], function () {
    Route::get('login', 'LoginController@loginForm')->name('login');
    Route::post('login', 'LoginController@handleLogin');

    Route::get('logout', 'LogoutController@logOut')->name('logout');

    Route::get('forgot-password', 'ForgotPasswordController@forgotPasswordForm')->name('forgot_password');
    Route::post('forgot-password', 'ForgotPasswordController@handleForgotPasswordRequest');

    Route::get('reset-password', 'ResetPasswordController@formResetPassword')->name('reset_password');
    Route::post('reset-password', 'ResetPasswordController@handleResetPasswordRequest');

    Route::get('login/google', 'LoginController@redirectToProvider')->name('login.google');
    Route::get('login/google/callback', 'LoginController@handleProviderCallback');

});

Route::group([
    'namespace' => 'Admin',
    'middleware' => ['auth', 'permission', /*'auth_logs'*/]
], function () {
    Route::get('/',[\App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('dashboard');

    //Route User
    Route::group([
        'prefix' => 'users'
    ], function () {
        Route::get('/password')->name('resetPassword');
        Route::get('/', [UserController::class, 'index'])
            ->name('users.index');
        Route::get('/{id}/edit', [UserController::class, 'edit'])
            ->name('users.edit');
        Route::post('/{id}/edit', [UserController::class, 'update'])
            ->name('users.update');
        Route::delete('/{id}/destroy/', [UserController::class, 'destroy'])
            ->name('users.destroy');
    });
    // Route Cms Account
    Route::group([
        'prefix' => 'cms-accounts'
    ], function () {
        Route::get('/', [CmsAccountController::class, 'index'])
            ->name('cms-account.index');
        Route::get('/create', [CmsAccountController::class, 'create'])
            ->name('cms-account.create');
        Route::post('/create', [CmsAccountController::class, 'store'])
            ->name('cms-account.store');
        Route::get('/{id}/edit', [CmsAccountController::class, 'edit'])
            ->name('cms-account.edit');
        Route::post('/{id}/edit', [CmsAccountController::class, 'update'])
            ->name('cms-account.update');
        Route::delete('/{id}/destroy/', [CmsAccountController::class, 'destroy'])
            ->name('cms-account.destroy');
    });

    // Route Permission
    Route::group([
        'prefix' => 'permissions'
    ], function () {
        Route::get('/', [PermissionController::class, 'index'])
            ->name('permission.index');
        Route::get('/create', [PermissionController::class, 'create'])
            ->name('permission.create');
        Route::post('/create', [PermissionController::class, 'store'])
            ->name('permission.store');
        Route::get('/{id}/edit', [PermissionController::class, 'edit'])
            ->name('permission.edit');
        Route::post('/{id}/edit', [PermissionController::class, 'update'])
            ->name('permission.update');
        Route::delete('/{id}/destroy/', [PermissionController::class, 'destroy'])
            ->name('permission.destroy');
    });
    //Route Role
    Route::group([
        'prefix' => 'roles'
    ], function () {
        Route::get('/', [RoleController::class, 'index'])
            ->name('role.index');
        Route::get('/create', [RoleController::class, 'create'])
            ->name('role.create');
        Route::post('/create', [RoleController::class, 'store'])
            ->name('role.store');
        Route::get('/{id}/edit', [RoleController::class, 'edit'])
            ->name('role.edit');
        Route::post('/{id}/edit', [RoleController::class, 'update'])
            ->name('role.update');
        Route::delete('/{id}/destroy/', [RoleController::class, 'destroy'])
            ->name('role.destroy');
        Route::get('/sync', [RoleController::class, 'sync'])
            ->name('role.sync');
    });

    // Route Category
    Route::group([
        'prefix' => 'category'
    ], function () {
        Route::get('/', [CategoryController::class, 'index'])
            ->name('category.index');
        Route::get('/create', [CategoryController::class, 'create'])
            ->name('category.create');
        Route::post('/create', [CategoryController::class, 'store'])
            ->name('category.store');
        Route::get('/{id}/edit', [CategoryController::class, 'edit'])
            ->name('category.edit');
        Route::post('/{id}/edit', [CategoryController::class, 'update'])
            ->name('category.update');
        Route::delete('/{id}/destroy/', [CategoryController::class, 'destroy'])
            ->name('category.destroy');
    });
    // Route Category
    Route::group([
        'prefix' => 'news'
    ], function () {
        Route::get('/', [NewController::class, 'index'])
            ->name('news.index');
        Route::get('/create', [NewController::class, 'create'])
            ->name('news.create');
        Route::post('/create', [NewController::class, 'store'])
            ->name('news.store');
        Route::get('/show', [NewController::class, 'show'])
            ->name('news.show');
        Route::get('/{id}/edit', [NewController::class, 'edit'])
            ->name('news.edit');
        Route::post('/{id}/edit', [NewController::class, 'update'])
            ->name('news.update');
        Route::delete('/{id}/destroy/', [NewController::class, 'destroy'])
            ->name('news.destroy');
        Route::get('/{id}/approve', [NewController::class, 'approve'])
            ->name('news.approve');
        Route::get('/{id}/publish', [NewController::class, 'publish'])
            ->name('news.publish');
        Route::post('/update-status', [NewController::class, 'updateStatus'])
            ->name('news.update-status');
    });

    //Vendor
    Route::group([
        'prefix' => 'vendors'
    ], function () {
        Route::get('/', [VendorController::class, 'index'])
            ->name('vendor.index');
        Route::get('/create', [VendorController::class, 'create'])
            ->name('vendor.create');
        Route::post('/create', [VendorController::class, 'store'])
            ->name('vendor.store');
        Route::get('/show', [VendorController::class, 'show'])
            ->name('vendor.show');
        Route::get('/{id}/edit', [VendorController::class, 'edit'])
            ->name('vendor.edit');
        Route::post('/{id}/edit', [VendorController::class, 'update'])
            ->name('vendor.update');
        Route::delete('/{id}/destroy/', [VendorController::class, 'destroy'])
            ->name('vendor.destroy');
    });

     Route::group([
         'prefix' => 'config'
     ], function () {
         Route::get('/', [ConfigController::class, 'index'])
             ->name('config.index');
         Route::get('/create', [ConfigController::class, 'create'])
             ->name('config.create');
         Route::post('/create', [ConfigController::class, 'store'])
             ->name('config.store');
         Route::get('/{id}/edit', [ConfigController::class, 'edit'])
             ->name('config.edit');
         Route::post('/{id}/edit', [ConfigController::class, 'update'])
             ->name('config.update');
         Route::delete('/delete/{id}', [ConfigController::class, 'destroy'])
             ->name('config.destroy');
     });

    // Route Pathogen
    Route::group([
        'prefix' => 'pathogen'
    ], function () {
        Route::get('/', [PathogenController::class, 'index'])
            ->name('pathogen.index');
        Route::get('/create', [PathogenController::class, 'create'])
            ->name('pathogen.create');
        Route::post('/create', [PathogenController::class, 'store'])
            ->name('pathogen.store');
        Route::get('/{id}/edit', [PathogenController::class, 'edit'])
            ->name('pathogen.edit');
        Route::post('/{id}/edit', [PathogenController::class, 'update'])
            ->name('pathogen.update');
        Route::delete('/{id}/destroy/', [PathogenController::class, 'destroy'])
            ->name('pathogen.destroy');
    });

    // Route Location Medicine
    Route::group([
        'prefix' => 'medicine-location'
    ], function () {
        Route::get('/', [MedicineLocationController::class, 'index'])
            ->name('medicine-location.index');
        Route::get('/create', [MedicineLocationController::class, 'create'])
            ->name('medicine-location.create');
        Route::post('/create', [MedicineLocationController::class, 'store'])
            ->name('medicine-location.store');
        Route::get('/{id}/edit', [MedicineLocationController::class, 'edit'])
            ->name('medicine-location.edit');
        Route::post('/{id}/edit', [MedicineLocationController::class, 'update'])
            ->name('medicine-location.update');
        Route::delete('/{id}/destroy/', [MedicineLocationController::class, 'destroy'])
            ->name('medicine-location.destroy');
    });
    // Route Vaccine
    Route::group([
        'prefix' => 'vaccine'
    ], function () {
        Route::get('/', [VaccineController::class, 'index'])
            ->name('vaccine.index');
        Route::get('/create', [VaccineController::class, 'create'])
            ->name('vaccine.create');
        Route::post('/create', [VaccineController::class, 'store'])
            ->name('vaccine.store');
        Route::get('/{id}/edit', [VaccineController::class, 'edit'])
            ->name('vaccine.edit');
        Route::post('/{id}/edit', [VaccineController::class, 'update'])
            ->name('vaccine.update');
        Route::delete('/{id}/destroy/', [VaccineController::class, 'destroy'])
            ->name('vaccine.destroy');
    });
    Route::group([
        'prefix' => 'notification'
    ], function () {
        Route::get('/', [NotificationController::class, 'index'])
            ->name('notification.index');
        Route::get('/create', [NotificationController::class, 'create'])
            ->name('notification.create');
        Route::post('/create', [NotificationController::class, 'store'])
            ->name('notification.store');
    });
    // Route Popup
    Route::group([
        'prefix' => 'popup'
    ], function () {
        Route::get('/', [PopupController::class, 'index'])
            ->name('popup.index');
        Route::get('/create', [PopupController::class, 'create'])
            ->name('popup.create');
        Route::post('/create', [PopupController::class, 'store'])
            ->name('popup.store');
        Route::get('/{id}/edit', [PopupController::class, 'edit'])
            ->name('popup.edit');
        Route::post('/{id}/edit', [PopupController::class, 'update'])
            ->name('popup.update');
        Route::delete('/{id}/destroy/', [PopupController::class, 'destroy'])
            ->name('popup.destroy');
    });
    Route::prefix('vaccine_calendar')->group(function () {
        Route::get('/', [VaccineCalendarController::class, 'index'])->name('vaccine_calendar');
    });

    Route::prefix('vaccine_book')->group(function () {
        Route::get('/', [VaccineBookController::class, 'index'])->name('vaccine_book');
    });

    Route::prefix('vaccine_note')->group(function () {
        Route::get('/', [VaccineNoteController::class, 'index'])->name('vaccine_note');
    });
    Route::group([
        'prefix' => 'order'
    ], function () {
        Route::get('/', [OrderController::class, 'index'])
            ->name('order.index');
        Route::get('{id}/detail', [OrderController::class, 'detail'])
            ->name('order.detail');
        Route::post('{id}/set-finish-status', [OrderController::class, 'setFinishStatus'])
            ->name('order.finish-status');
        Route::post('{id}/set-failed-status', [OrderController::class, 'setFailedStatus'])
            ->name('order.failed-status');
    });
});

<?php

namespace App\Http\Middleware;

use App\Helpers\PermissionsHelper;
use App\Repositories\Contracts\CmsAccountRepositoryInterface;
use App\Repositories\Contracts\PermissionRepositoryInterface;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class CheckPermissions
{


    public function handle($request, Closure $next)
    {
//        return $next($request);

        $result = PermissionsHelper::can($request->route()->getName());
        if ($result) {
            return $next($request);
        }
        if($request->ajax()){
            return response()->json([
                'status' => 'error',
                'message' => 'Bạn không đủ quyền truy cập vào trang này'
            ]);
        }
        return response()->view('auth.message', [
            'message' => 'Bạn không đủ quyền truy cập vào trang này'
        ]);
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateVaccine extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(Auth::check()) return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'secret_key' => 'max:32',
            'avatar' => 'required',
            'name' => 'unique:vaccine,'.$this->id.',_id|required|max:100|min:1',
            'vaccine_code' => 'unique:vaccine,'.$this->id.',_id|required|max:100|min:1',
            'pathogen_ids' => '',
            'brand_name' => '',
            'fulltext' => '',
            'prescription'=>'',
            'day_age' => '',
            'week_age' => '',
            'month_age' => '',
            'ages' => ''
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class VaccineRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(Auth::check()) return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'secret_key' => 'max:32',
            'avatar' => 'required',
            'name' => 'required|max:255|unique:vaccine',
            'vaccine_code' => 'required|max:255|unique:vaccine',
            'pathogen_ids' => '',
            'brand_name' => '',
            'fulltext' => '',
            'day_age' => '',
            'week_age' => '',
            'month_age' => '',
            'ages' => ''
        ];
        if ($this->id) {
            $rules['name'] .= ",{$this->id},_id";
            $rules['vaccine_code'] .= ",{$this->id},_id";
        }
        return $rules;
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CmsAccountsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'status' => 'required|max:255|in:0,1',
            'full_name' => 'required|max:255',
            'roles' => 'required',
            'avatar' => '',
        ];

        if ($this->id) {
            $rules['email'] = "required|unique:cms_accounts,email,{$this->id},_id";

            return $rules;
        }
        $rules['avatar'] .= '|required';
        $rules['email'] = "required|unique:cms_accounts";
        $rules ['password'] = 'required|min:8';

        return $rules;
    }
}

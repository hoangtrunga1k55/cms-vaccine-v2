<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class MedicineLocationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(Auth::check()) return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'secret_key' => 'max:32',
            'name' => 'required|max:255|unique:medicine_locations',
            'lat' => 'required|numeric',
            'lng' => 'required|numeric',
            'description' => 'string',
            'phone_number' => 'string'
        ];
        if ($this->id) {
            $rules['name'] .= ",{$this->id},_id";

        }
        return $rules;
    }
}

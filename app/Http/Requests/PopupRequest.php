<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PopupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'unique:popup|required|max:255',
            'status' => '',
            'content' => 'required|string',
            'positions' => 'required|array',
            'post_id' => '',
            'image' => ''
        ];
        if(!is_null($this->id)){
            $rules['title'] .= ",{$this->id},_id";
            return $rules;
        }
        $rules['image'] .= '|required';
        return $rules;
    }
}

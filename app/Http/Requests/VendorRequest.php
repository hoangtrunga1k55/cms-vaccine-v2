<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VendorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'description' => 'required',
            'status' => 'required|in:0,1',
            'image' => ''
        ];
        if(!is_null($this->id)){
            return $rules;
        }
        $rules['image'] .= '|required';
        return $rules;
    }
    public function messages()
    {
        return [
            'title.required' => 'Vui lòng nhâp tên nhà cung cấp',
            'title.max' => 'Tên nhà cung cấp phải nhỏ hơn 255 ký tự',
            'description.required' => 'Vui lòng nhập mô tả',
            'status.in' => 'Vui lòng nhập đúng định dạng 0,1',
            'image.required' => 'Vui lòng nhập hình ảnh',
            'image.image'=>'Chỉ chấp nhận file ảnh'
        ];
    }

}

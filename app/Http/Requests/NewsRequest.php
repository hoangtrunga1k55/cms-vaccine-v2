<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|max:70',
            'description' => 'required|max:160',
            'meta_title' => 'max:70',
            'meta_description' => 'max:160',
            'meta_keywords' => '',
            'meta_thumbnail' => '',
            'content' => 'required',
            'priority' => '',
            'published_at' => '',
            'category_ids' => '',
            'vendor_ids' => '',
            'tag_ids' => '',
            'image' => ''
        ];
        if (!empty($this->id)) {
            return $rules;
        }
        $rules['image'] .= '|required';
        return $rules;
    }

    public function messages()
    {
        return [
            'title.required' => 'Vui lòng nhâp tiêu đề',
            'title.max' => 'Vui lòng nhâp ít hơn 70 ký tự',
            'meta_title.max' => 'Vui lòng nhâp ít hơn 70 ký tự',
            'meta_description.max' => 'Vui lòng nhâp ít hơn 160 ký tự',
            'description.required' => 'Vui lòng nhập mô tả ngắn',
            'description.max' => 'Vui lòng nhập ít hơn 160 ký tự',
            'content.required' => 'Vui lòng nhập nội dung',
            'image.required' => 'Vui lòng nhập hình ảnh',
            'image.image' => 'Chỉ chấp nhận file ảnh'
        ];
    }
}

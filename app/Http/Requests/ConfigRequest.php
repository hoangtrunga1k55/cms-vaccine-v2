<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConfigRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|max:255',
            'key' => 'required|max:255',
            'status' => 'required|max:255',
            'content' => 'required',
        ];
        return $rules;
    }
    public function messages()
    {
        return [
            'title.required' => 'A title is required',
            'title.max' => 'Max of character is 255',
            'key.required' => 'A key is required',
            'key.max' => 'Max of character is 255',
            'content.required' => 'A Content is required',
            'status.max' => 'Max of status is 255',
        ];
    }

}

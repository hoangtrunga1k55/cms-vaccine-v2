<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'priority' => '',
            'status' => 'required|in:0,1',
            'description'=>'max:255',
            'parent_id' => '',
            'meta_title' => 'max:70',
            'meta_description' => 'max:160',
            'meta_keywords' => '',
            'meta_thumbnail'=>''
        ];
        return $rules;
    }
    public function messages()
    {
        return [
            'name.required' => 'Vui lòng nhâp tên danh mục',
            'description.max' => 'Mô tả không quá 255 ký tự',
            'meta_description.max' => 'Meta_description không quá 160 ký tự',
            'meta_title.max' => 'Meta_title không quá 70 ký tự',
        ];
    }
}

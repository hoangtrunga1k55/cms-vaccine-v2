<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ResetPasswordRequest;
use App\Repositories\Contracts\CmsAccountRepositoryInterface;
use App\Repositories\Contracts\PasswordResetRepositoryInterface;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    protected $cmsAccountRepository;
    protected $passwordResetRepository;

    public function __construct(CmsAccountRepositoryInterface $cmsAccountRepository, PasswordResetRepositoryInterface $passwordResetRepository)
    {
        $this->cmsAccountRepository = $cmsAccountRepository;
        $this->passwordResetRepository = $passwordResetRepository;
    }

    public function formResetPassword(Request $request)
    {
        $email = $this->passwordResetRepository->findBy('token', $request->token);

        if(is_null($email)){
            return view('auth.message', [
                'message' => 'Đường dẫn không hợp lệ'
            ]);
        }
        return view('auth.reset-password', [
            'token' => $request->token,
        ]);
    }

    public function handleResetPasswordRequest(ResetPasswordRequest $request){
        $passwordReset = $this->passwordResetRepository->findBy('token', $request->reset_token);
        if(is_null($passwordReset)){
            return view('auth.message', [
                'message' => 'Đường dẫn không hợp lệ'
            ]);
        }

        $this->cmsAccountRepository
            ->updateOrCreate(
                ['email' => (string) $passwordReset->email],
                ['password' => bcrypt($request->password)]
            );

        $this->passwordResetRepository->deleteAllBy(['token' => $request->reset_token]);

        return view('auth.message', [
            'message' => 'Đổi mật khẩu thành công'
        ]);
    }
}

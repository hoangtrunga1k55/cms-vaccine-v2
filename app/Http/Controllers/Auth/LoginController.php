<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\CmsAccountRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    protected $cmsAccountRepository;

    public function __construct(CmsAccountRepositoryInterface $cmsAccountRepository)
    {
        $this->cmsAccountRepository = $cmsAccountRepository;
    }

    public function loginForm()
    {
        return view('auth.login');
    }

    public function handleLogin(Request $request)
    {
        Cache::forget('permissions');
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            if (Auth::user()->status == 0) {
                Cache::tags('permissions')->forget(Auth::id());
                Auth::logout();
                return back()->withErrors('Tài khoản đã bị khóa');
            }
            return redirect()->intended(route('dashboard'));
        }
        return back()->withErrors('Xác thực tài khoản thất bại, vui lòng kiểm tra lại email và mật khẩu');
    }

    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleProviderCallback(Request $request)
    {
        $googleUser = Socialite::driver('google')->stateless()->user();
        // dd($googleUser);

        $authUser = $this->cmsAccountRepository->findBy('email', $googleUser->getEmail());

        if(!isset($authUser)){
            return view('auth.message', [
                'message' => 'Tài khoản này chưa được tạo trên hệ thống'
            ]);
        }

        if($authUser->status == 0){
            return view('auth.message', [
                'message' => 'Tài khoản đã bị khóa'
            ]);
        }
        Auth::login($authUser);
        Cache::tags('permissions')->forget($authUser->id);

        return redirect()->route('dashboard');

    }
}

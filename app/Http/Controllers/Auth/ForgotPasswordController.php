<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Jobs\ProcessSendResetPasswordMail;
use App\Mail\Auth\ResetPassword;
use App\Repositories\Contracts\CmsAccountRepositoryInterface;
use App\Repositories\Contracts\PasswordResetRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class ForgotPasswordController extends Controller
{
    protected $cmsAccountRepository;
    protected $passwordResetRepository;

    public function __construct(CmsAccountRepositoryInterface $cmsAccountRepository, PasswordResetRepositoryInterface $passwordResetRepository)
    {
        $this->cmsAccountRepository = $cmsAccountRepository;
        $this->passwordResetRepository = $passwordResetRepository;
    }

    public function forgotPasswordForm()
    {
        return view('auth.forgot-password');
    }

    public function handleForgotPasswordRequest(Request $request)
    {
        $user = $this->cmsAccountRepository
            ->findBy('email', $request->email);

        if (is_null($user)) {
            return view('auth.message', [
                'message' => 'Không thể tìm thấy email yêu cầu'
            ]);
        }
        if ($user->status == 0) {
            return view('auth.message', [
                'message' => 'Tài khoản này đã bị khóa'
            ]);
        }

        $token = (string)Str::uuid();

        $attributes = [
            'token' => $token,
            'email' => $user->email
        ];

        $this->passwordResetRepository->updateOrCreate([
            'email' => $user->email,
        ], $attributes);

        ProcessSendResetPasswordMail::dispatch($token, $user->email);
//        Mail::to($user->email)
//            ->send(new ResetPassword($token));

        return view('auth.message', [
            'message' => 'Vui lòng kiểm tra hòm thư và làm theo hướng dẫn, hãy kiểm tra cả thùng thư spam nếu không thấy mal gửi về'
        ]);
    }


}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\CmsAccountRepositoryInterface;
use App\Repositories\Contracts\NotificationRepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Repositories\Eloquents\NewsRepository;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    protected $cmsAccountRepository;
    protected $userRepository;
    protected $newsRepository;
    protected $notificationRepository;

    public function __construct(CmsAccountRepositoryInterface $cmsAccountRepository,
                                UserRepositoryInterface $userRepository,
                                NotificationRepositoryInterface $notificationRepository,
                                NewsRepository $newsRepository)
    {
        $this->cmsAccountRepository = $cmsAccountRepository;
        $this->userRepository = $userRepository;
        $this->newsRepository = $newsRepository;
        $this->notificationRepository = $notificationRepository;

    }

    public function index()
    {
        $countUser = $this->userRepository->all()->count();
        $countVaccine = DB::table('vaccine')->get()->count();
        $countNew = DB::table('new')->get()->count();
        $orderBy = array('created_at' => 'asc');
        $nru = $this->userRepository->all(array('*'), [], [], $orderBy)
            ->groupBy(function ($date) {
                return Carbon::parse($date->created_at)->format('d-m-Y');
            });
        $articles = $this->newsRepository->all(array('*'), [], [], $orderBy)
            ->groupBy(function ($date) {
                return Carbon::parse($date->created_at)->format('d-m-Y');
            });
        $notify = $this->notificationRepository->all(array('*'), [], [], $orderBy)
            ->groupBy(function ($date) {
                return Carbon::parse($date->created_at)->format('d-m-Y');
            });
        return view('admin.pages.dashboard', compact(
            'countUser', 'countVaccine', 'countNew', 'nru', 'articles', 'notify'
        ));
    }
}

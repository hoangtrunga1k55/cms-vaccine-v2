<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ImageHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\VendorRequest;
use App\Repositories\Contracts\VendorRepositoryInterface;
use Illuminate\Http\Request;

class VendorController extends Controller
{

    protected $vendorRepository;

    public function __construct(VendorRepositoryInterface $vendorRepository)
    {
        $this->vendorRepository = $vendorRepository;
    }

    public function index(Request $request)
    {
        $condition=[];
        if (!is_null($request->status)){
            $condition['where'][] = ['status','like',$request->status];
        }
        $vendors = $this->vendorRepository->paginateList(10,$condition);
        return view('admin.pages.vendor.list',compact(
            'vendors','request'
        ));
    }

    public function create()
    {
        return view('admin.pages.vendor.form');
    }


    public function store(VendorRequest $request)
    {
        $this->handleSubmitRequest($request);

        return response()->json([
            'message' => 'Tạo thành công',
            'status' => 'success',
            'url' => route('vendor.index')
        ]);
    }

    public function edit($id)
    {
        $vendor = $this->vendorRepository->findById($id);
        return view('admin.pages.vendor.form', compact('vendor'));
    }

    public function update(VendorRequest $request, $id)
    {
        $this->handleSubmitRequest($request, $id);

        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('vendor.index')
        ]);
    }


    public function destroy($id)
    {
        $this->vendorRepository->destroy($id);

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }


    public function handleSubmitRequest($request, $id = null)
    {
        $attributes = $request->validated();
        $attributes['status'] = (int)$attributes['status'];


        $image = ImageHelper::upload($request->file('image'), '/uploads/vendors/');
        if ($image != ImageHelper::$NOTFOUND) {
            $attributes['image'] = $image;
        }

        if (!is_null($id)) {
            $this->vendorRepository->update($attributes, $request->id);
        } else {
            $this->vendorRepository->create($attributes);
        }

    }
}

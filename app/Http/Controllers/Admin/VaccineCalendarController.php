<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UpdateVaccineCalendar;
use App\Repositories\Contracts\PathogenRepositoryInterface;
use App\Repositories\Contracts\VaccineRepositoryInterface;
use App\Http\Controllers\Controller;

class VaccineCalendarController extends Controller
{
    protected $pathogenRepository;
    protected $vaccineRepository;

    public function __construct(VaccineRepositoryInterface $vaccineRepository, PathogenRepositoryInterface $pathogenRepository)
    {
        $this->vaccineRepository = $vaccineRepository;
        $this->pathogenRepository = $pathogenRepository;
    }

    public function index()
    {

        $vaccines = $this->vaccineRepository->all();
        $pathogens = $this->pathogenRepository->all();
        $calendar = [];

        if (file_exists(public_path('vaccine_calendar.json'))) {
            $calendar = file_get_contents(public_path('vaccine_calendar.json'));
            $calendar = json_decode($calendar, true);
        }

        return view('admin.pages.vaccine_calendar.form', compact(
            'vaccines',
            'pathogens',
            'calendar'));
    }

    public function save(UpdateVaccineCalendar $request)
    {
        $data = $request->validated();
        file_put_contents(public_path('vaccine_calendar.json'), json_encode($data));
        return $data;
    }
}

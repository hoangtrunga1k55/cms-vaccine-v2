<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PathogenRequest;
use App\Models\Pathogen;
use App\Repositories\Contracts\PathogenRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PathogenController extends Controller
{
    protected $pathogenRepository;

    public function __construct(PathogenRepositoryInterface $pathogenRepository)
    {
        $this->pathogenRepository = $pathogenRepository;
    }

    public function index(Request $request)
    {
        $conditions = [];
        if (!is_null($request->name)) {
            $conditions['where'][] = ['name', 'like', "%" . $request->name . "%"];
        }

        $pathogens = $this->pathogenRepository->paginateList(10, $conditions, [], []);
        return view('admin.pages.pathogen.index', compact(
            'pathogens',
            'request'
        ));
    }

    public function create()
    {
        return view('admin.pages.pathogen.form');
    }

    public function store(PathogenRequest $request)
    {
        $data = $request->validated();

        $this->pathogenRepository->create($data);

        return response()->json([
            'message' => 'Thêm mới thành công',
            'status' => 'success',
            'url' => route('pathogen.index')
        ]);
    }

    public function edit($id)
    {
        $pathogen = $this->pathogenRepository->findById($id);

        return view('admin.pages.pathogen.form', compact('pathogen'));
    }

    public function update(PathogenRequest $request, $id)
    {
        $data = $request->validated();

        $this->pathogenRepository->update($data, $id);

        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('pathogen.index')
        ]);
    }

    public function destroy($id)
    {
        $item = $this->pathogenRepository->findById($id);
        if ($item) {
            $item->delete();
        }
        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }
}

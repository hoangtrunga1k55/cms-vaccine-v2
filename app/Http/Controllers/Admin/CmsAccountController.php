<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CmsAccountsRequest;
use App\Models\CmsAccount;
use App\Repositories\Contracts\CmsAccountRepositoryInterface;
use App\Repositories\Contracts\RoleRepositoryInterface;
use Illuminate\Http\Request;
use App\Helpers\ImageHelper;
use Illuminate\Support\Facades\Cache;

class CmsAccountController extends Controller
{
    protected $cmsAccountRepository;
    protected $roleRepository;

    public function __construct(CmsAccountRepositoryInterface $cmsAccountRepository, RoleRepositoryInterface $roleRepository)
    {
        $this->cmsAccountRepository = $cmsAccountRepository;
        $this->roleRepository = $roleRepository;
    }


    public function index(Request $request)
    {
        $conditions = [];
        if ($request->email) {
            $conditions['where'][] = ['email', 'like', "%" . $request->email . "%"];
        }

        $cmsAccounts = $this->cmsAccountRepository->paginateList(5, $conditions);
        return view('admin.pages.cms-account.index', compact(
            'cmsAccounts',
            'request'
        ));
    }

    public function create()
    {
        $roles = $this->roleRepository->all();
        return view('admin.pages.cms-account.form', compact(
            'roles'
        ));
    }

    public function store(CmsAccountsRequest $request)
    {
        $attributes = $request->validated();
        unset($attributes['roles']);

        $attributes['password'] = bcrypt($request->password);
        $avatar = ImageHelper::upload($request->file('avatar'), 'CmsAccountAvatar');
        if (!is_null($avatar)) {
            $attributes['avatar'] = $avatar;
        }

        $this->cmsAccountRepository
            ->create($attributes)
            ->roles()
            ->sync($request->roles);

        Cache::forget('permissions');
        return redirect()->route('cms-account.index');
    }

    public function edit($id)
    {
        $cmsAccount = $this->cmsAccountRepository->findById($id);
        $roles = $this->roleRepository->all();

        if (empty($cmsAccount)) return redirect()->route('cms_account.index');

        return view('admin.pages.cms-account.form', compact(
            'cmsAccount',
            'roles'
        ));
    }

    public function update(CmsAccountsRequest $request, $id)
    {
        $attributes = $request->validated();
        unset($attributes['roles']);

//        $avatar = ImageHelper::upload($request->file('avatar'), 'CmsAccountAvatar');
//        if (!is_null($avatar)) {
//            $attributes['avatar'] = $avatar;
//        }
        $this->cmsAccountRepository
            ->update($attributes, $id);
        $this->cmsAccountRepository
            ->findById($id)
            ->roles()
            ->sync($request->roles);

        Cache::tags('permissions')->forget($request->id);
        return redirect()->route('cms-account.index');
    }

    public function destroy($id)
    {
        $this->cmsAccountRepository->destroy($id);
        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StorePathogen;
use App\Http\Requests\UpdatePathogen;
use App\Models\Pathogen;
use App\Repositories\Contracts\PathogenRepositoryInterface;
use App\Repositories\Contracts\VaccineRepositoryInterface;
use App\Repositories\Contracts\VaccineBookRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VaccineBookController extends Controller
{
    protected $vaccine_book;

    public function __construct(VaccineBookRepositoryInterface $vaccine_book)
    {
        $this->vaccine_book = $vaccine_book;
    }

    public function index(Request $request)
    {
        $conditions = [];
        $user_full_name = null;
        if($request->user_full_name){
            $user_full_name = $request->user_full_name;
            $conditions['where'][] = ['user_full_name', 'like', '%' . $user_full_name . '%'];
        }
        if($request->dob_from){
            $user_full_name = $request->user_full_name;
            $conditions['where'][] = ['user_full_name', 'like', '%' . $user_full_name . '%'];
        }
        if($request->dob_to){
            $user_full_name = $request->user_full_name;
            $conditions['where'][] = ['user_full_name', 'like', '%' . $user_full_name . '%'];
        }

        $vaccine_books = $this->vaccine_book->paginateList(10, $conditions, ['user_info'], []);
        return view('admin.pages.vaccine_book.list', compact('vaccine_books', 'user_full_name', "request"));
    }
}

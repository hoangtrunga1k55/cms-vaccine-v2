<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MedicineLocationRequest;
use App\Repositories\Contracts\MedicineLocationRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class MedicineLocationController extends Controller
{
    protected $medicineLocationRepository;

    public function __construct(MedicineLocationRepositoryInterface $medicineLocationRepository)
    {
        $this->medicineLocationRepository = $medicineLocationRepository;
    }

    public function index(Request $request)
    {
        $conditions = [];
        if (!is_null($request->name)) {
            $conditions['where'][] = ['name', 'like', $request->name];
        }
        if (!is_null($request->phone_number)) {
            $conditions['where'][] = ['phone_number', 'like', $request->phone_number];
        }

        $medicine_locations = $this->medicineLocationRepository
            ->paginateList(10, $conditions, [], ['created_at' => 'DESC']);

        return view('admin.pages.location_medicine.index', compact(
            'medicine_locations',
            'request'
        ));
    }

    public function create()
    {
        return view('admin.pages.location_medicine.form');
    }

    public function store(MedicineLocationRequest $request)
    {
        $data = $request->validated();

        $data['location'] = [
            'x' => (float)$data['lat'],
            'y' => (float)$data['lng']
        ];
        unset($data['lat']);
        unset($data['lng']);
        $this->medicineLocationRepository->create($data);

        return response()->json([
            'message' => 'Thêm thành công',
            'status' => 'success',
            'url' => route('medicine-location.index')
        ]);
    }

    public function edit($id)
    {
        $medicine_location = $this->medicineLocationRepository->findById($id);
        return view('admin.pages.location_medicine.form', compact('medicine_location'));
    }

    public function update(MedicineLocationRequest $request, $id)
    {
        $data = $request->validated();

        $data['location'] = [
            'x' => (float)$data['lat'],
            'y' => (float)$data['lng']
        ];
        unset($data['lat']);
        unset($data['lng']);
        $this->medicineLocationRepository->update($data, $id);

        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('medicine-location.index')
        ]);
    }

    public function destroy($id)
    {
        $item = $this->medicineLocationRepository->findById($id);
        if ($item) {
            $item->delete();
        }
        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }
}

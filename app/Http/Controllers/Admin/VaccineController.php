<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreVaccine;
use App\Http\Requests\UpdateVaccine;
use App\Models\Vaccine;
use App\Repositories\Contracts\PathogenRepositoryInterface;
use App\Repositories\Contracts\VaccineRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class VaccineController extends Controller
{
    protected $pathogenRepository;
    protected $vaccineRepository;

    public function __construct(VaccineRepositoryInterface $vaccineRepository, PathogenRepositoryInterface $pathogenRepository)
    {
        $this->vaccineRepository = $vaccineRepository;
        $this->pathogenRepository = $pathogenRepository;
    }

    public function index(Request $request)
    {
        $conditions = [];
        if (!is_null($request->name)) {
            $conditions['where'][] = ['name', 'like', "%" . $request->name . "%"];
        }


        $vaccines = $this->vaccineRepository->paginateList(10, $conditions, [], []);
        return view('admin.pages.vaccine.index', compact('vaccines', 'request'));
    }

    public function handeDataVaccine($vaccine = null)
    {
        $data = [];
        $data['pathogens'] = $this->pathogenRepository->all(['_id', 'name']);
        $data['formAction'] = isset($vaccine) ? route('vaccine.update', $vaccine->id) : route('vaccine.store');
        $data['csrf_token'] = csrf_token();
        $data['dataVaccine'] = [
            'avatar' => isset($vaccine->avatar) ? $vaccine->avatar : old('avatar'),
            'name' => isset($vaccine->name) ? $vaccine->name : old('name'),
            'vaccine_code' => isset($vaccine->vaccine_code) ? $vaccine->vaccine_code : old('vaccine_code'),
            'pathogen_ids' => isset($vaccine->pathogen_ids) ? $vaccine->pathogen_ids : old('pathogen_ids'),
            'brand_name' => isset($vaccine->brand_name) ? $vaccine->brand_name : old('brand_name'),
            'fulltext' => isset($vaccine->fulltext) ? $vaccine->fulltext : old('fulltext'),
            'prescription' => isset($vaccine->prescription) ? $vaccine->prescription : old('prescription'),
            'ages' => isset($vaccine->ages) ? $vaccine->ages : old('ages'),
            'day_age' => isset($vaccine->day_age) ? $vaccine->day_age : old('day_age'),
            'week_age' => isset($vaccine->week_age) ? $vaccine->week_age : old('week_age'),
            'month_age' => isset($vaccine->month_age) ? $vaccine->month_age : old('month_age'),
            'time_turn_1_to_turn_2' => isset($vaccine->time_turn_1_to_turn_2) ? $vaccine->time_turn_1_to_turn_2 : old('time_turn_1_to_turn_2'),
            'unit_1_2' => isset($vaccine->unit_1_2) ? $vaccine->unit_1_2 : old('unit_1_2'),
            'time_turn_2_to_turn_3' => isset($vaccine->time_turn_2_to_turn_3) ? $vaccine->time_turn_2_to_turn_3 : old('time_turn_2_to_turn_3'),
            'unit_2_3' => isset($vaccine->unit_2_3) ? $vaccine->unit_2_3 : old('unit_2_3'),
            'time_turn_3_to_turn_4' => isset($vaccine->time_turn_3_to_turn_4) ? $vaccine->time_turn_3_to_turn_4 : old('time_turn_3_to_turn_4'),
            'unit_3_4' => isset($vaccine->unit_3_4) ? $vaccine->unit_3_4 : old('unit_3_4'),
            'time_turn_4_to_turn_5' => isset($vaccine->time_turn_4_to_turn_5) ? $vaccine->time_turn_4_to_turn_5 : old('time_turn_4_to_turn_5'),
            'unit_4_5' => isset($vaccine->unit_4_5) ? $vaccine->unit_4_5 : old('unit_4_5'),
            'time_turn_5_to_turn_6' => isset($vaccine->time_turn_5_to_turn_6) ? $vaccine->time_turn_5_to_turn_6 : old('time_turn_5_to_turn_6'),
            'unit_5_6' => isset($vaccine->unit_5_6) ? $vaccine->unit_5_6 : old('unit_5_6'),
            'time_turn_6_to_turn_7' => isset($vaccine->time_turn_6_to_turn_7) ? $vaccine->time_turn_6_to_turn_7 : old('time_turn_6_to_turn_7'),
            'unit_6_7' => isset($vaccine->unit_6_7) ? $vaccine->unit_6_7 : old('unit_6_7'),
            'time_turn_7_to_turn_8' => isset($vaccine->time_turn_7_to_turn_8) ? $vaccine->time_turn_7_to_turn_8 : old('time_turn_7_to_turn_8'),
            'unit_7_8' => isset($vaccine->unit_7_8) ? $vaccine->unit_7_8 : old('unit_7_8'),
            'time_turn_8_to_turn_9' => isset($vaccine->time_turn_8_to_turn_9) ? $vaccine->time_turn_8_to_turn_9 : old('time_turn_8_to_turn_9'),
            'unit_8_9' => isset($vaccine->unit_8_9) ? $vaccine->unit_8_9 : old('unit_8_9'),
            'time_turn_9_to_turn_10' => isset($vaccine->time_turn_9_to_turn_10) ? $vaccine->time_turn_9_to_turn_10 : old('time_turn_9_to_turn_10'),
            'unit_9_10' => isset($vaccine->unit_9_10) ? $vaccine->unit_9_10 : old('unit_9_10')
        ];
        $data['vendor_noti'] = '';
        return json_encode($data);
    }

    public function create()
    {
        $pathogens = $this->handeDataVaccine();
        return view('admin.pages.vaccine.form', compact('pathogens'));
    }

    public function store(StoreVaccine $request)
    {
        $data = $request->validated();

        $data['pathogen_ids'] = explode(',', $data['pathogen_ids']);
        $data['ages'] = explode(',', $data['ages']);
        for ($i = 0; $i < count($data['ages']); $i++) {
            $data['ages'][$i] = (integer)$data['ages'][$i];
        }
        $data['day_age'] = explode(',', $data['day_age']);
        for ($i = 0; $i < count($data['day_age']); $i++) {
            $data['day_age'][$i] = (integer)$data['day_age'][$i];
        }
        $data['week_age'] = explode(',', $data['week_age']);
        for ($i = 0; $i < count($data['week_age']); $i++) {
            $data['week_age'][$i] = (integer)$data['week_age'][$i];
        }
        $data['month_age'] = explode(',', $data['month_age']);
        for ($i = 0; $i < count($data['month_age']); $i++) {
            $data['month_age'][$i] = (integer)$data['month_age'][$i];
        }
        $data['turn_inject'] = 0;

        for ($i = 2; $i <= 10; $i++) {
            $key_time = 'time_turn_' . ($i - 1) . '_to_turn_' . $i;
            $key_unit = 'unit_' . ($i - 1) . '_' . $i;
            if (!empty(request()->get($key_time))) {
                $data[$key_time] = (integer)request()->get($key_time);
            } else {
                $data[$key_time] = null;
            }
            if (!empty(request()->get($key_unit))) {
                $data[$key_unit] = request()->get($key_unit);
            } else {
                $data[$key_unit] = null;
            }
            if (!empty(request()->get($key_time)) && !empty(request()->get($key_unit))) {
                $data['turn_inject'] = $i;
            }
        }
        $data['creator_id'] = Auth::id();

        $this->vaccineRepository->create($data);

        return redirect()->route('vaccine.index')->with('success', 'Thêm thành công');
    }

    public function edit($id)
    {
        $vaccine = $this->vaccineRepository->findById($id);

        $vaccine->ages = implode(',', $vaccine->ages);
        $vaccine->day_age = $vaccine->day_age ? implode(',', $vaccine->day_age) : '';
        $vaccine->week_age = $vaccine->week_age ? implode(',', $vaccine->week_age) : '';
        $vaccine->month_age = $vaccine->month_age ? implode(',', $vaccine->month_age) : '';
        $vaccine->pathogen_ids = implode(',', $vaccine->pathogen_ids);
        $pathogens = $this->handeDataVaccine($vaccine);

        return view('admin.pages.vaccine.form', compact('vaccine', 'pathogens'));
    }

    public function update(UpdateVaccine $request, $id)
    {
        $data = $request->validated();
//        dd($data);
        $data['pathogen_ids'] = explode(',', $data['pathogen_ids']);

        $data['ages'] = explode(',', $data['ages']);
        for ($i = 0; $i < count($data['ages']); $i++) {
            $data['ages'][$i] = (integer)$data['ages'][$i];
        }

        $data['day_age'] = explode(',', $data['day_age']);

        for ($i = 0; $i < count($data['day_age']); $i++) {
            $data['day_age'][$i] = (integer)$data['day_age'][$i];
        }

        $data['week_age'] = explode(',', $data['week_age']);
        for ($i = 0; $i < count($data['week_age']); $i++) {
            $data['week_age'][$i] = (integer)$data['week_age'][$i];
        }

        $data['month_age'] = explode(',', $data['month_age']);
        for ($i = 0; $i < count($data['month_age']); $i++) {
            $data['month_age'][$i] = (integer)$data['month_age'][$i];
        }

        for ($i = 2; $i <= 10; $i++) {
            $key_time = 'time_turn_' . ($i - 1) . '_to_turn_' . $i;
            $key_unit = 'unit_' . ($i - 1) . '_' . $i;
            if (!empty(request()->get($key_time))) {
                $data[$key_time] = (integer)request()->get($key_time);
            } else {
                $data[$key_time] = null;
            }
            if (!empty(request()->get($key_unit))) {
                $data[$key_unit] = request()->get($key_unit);
            } else {
                $data[$key_unit] = null;
            }
            if (!empty(request()->get($key_time)) && !empty(request()->get($key_unit))) {
                $data['turn_inject'] = $i;
            }
        }

        $this->vaccineRepository->update($data, $id);


        return redirect()->route('vaccine.index')->with('success', 'Sửa thành công');
    }

    public function destroy($id)
    {
        $this->vaccineRepository->destroy($id);
        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }
}

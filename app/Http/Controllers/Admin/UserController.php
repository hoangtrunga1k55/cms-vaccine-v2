<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUser;
use App\Repositories\Contracts\UserRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $userRepository;
    protected $roleRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }


    public function index(Request $request)
    {
        $conditions = [];
        if ($request->email) {
            $conditions['where'][] = ['email', 'like', "%" . $request->email . "%"];
        }
        if ($request->phone_number) {
            $conditions['where'][] = ['phone_number', 'like', "%" . $request->phone_number . "%"];
        }
        if ($request->address) {
            $conditions['where'][] = ['address', 'like', "%" . $request->address . "%"];
        }

        $users = $this->userRepository->paginateList(10, $conditions);
        return view('admin.pages.user.index', compact(
            'users',
            'request'
        ));
    }

    public function edit($id)
    {
        $user = $this->userRepository->findById($id);
        if (empty($user)) return redirect()->route('users.index');

        return view('admin.pages.user.form', compact(
            'user'
        ));
    }

    public function update(UpdateUser $request, $id)
    {
        $attributes = $request->validated();
        if(!is_null($request->avatar)){
            $attributes['avatar'] = $request->avatar;
        }else{
            unset($attributes['avatar']);
        }
        if(!is_null($request->password)){
            $attributes['password'] = bcrypt($request->password);
        }else{
            unset($attributes['password']);
        }
        if (isset($attributes['birth_day'])) {
            $attributes['birth_day'] = (int) Carbon::createFromFormat('Y-m-d', $request->birth_day)
                ->timestamp;
        }
        $this->userRepository->update($attributes, $id);
        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('users.index')
        ]);
    }

    public function destroy($id)
    {
        $this->userRepository->destroy($id);
        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }
}

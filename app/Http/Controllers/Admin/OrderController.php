<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Repositories\Contracts\OrderRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{

    protected $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }


    public function index(Request $request)
    {
        $conditions = [];
        if (!is_null($request->name)) {
            $conditions['where'][] = ['full_name', 'like', "%" . $request->name . "%"];
        }

        $orders = $this->orderRepository->paginateList(10, $conditions);

        return view('admin.pages.order.index', compact(
            'orders', 'request'
        ));
    }

    public function detail($id)
    {
        $order = $this->orderRepository->findById($id);
        return response()->json([
            'view' => view('admin.pages.order._modal-detail', compact(
                'order'
            ))->render()
        ]);
    }

    public function setFinishStatus($id){
        $this->updateStatus(Order::STATUS_FINISH, $id);
        return response()->json([
            'status' => "success",
            'message' => "Cập nhật trạng thái thành công"
        ]);
    }

    public function setFailedStatus($id){
        $this->updateStatus(Order::STATUS_FAILED, $id);
        return response()->json([
            'status' => "success",
            'message' => "Cập nhật trạng thái thành công"
        ]);
    }
    public function updateStatus($status, $id){
        $this->orderRepository->update([
            'status' => $status,
            'updated_by' => Auth::id()
        ], $id);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ConfigRequest;
use App\Repositories\Contracts\ConfigRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConfigController extends Controller
{
    protected $configRepository;

    public function __construct(ConfigRepositoryInterface $configRepository)
    {
        $this->configRepository = $configRepository;
    }


    public function index(Request $request)
    {
        $conditions = [];
        if (!is_null($request->title)){
            $conditions['where'][] =['title','like','%'.$request->title.'%'];
        }
        if (!is_null($request->key)){
            $conditions['where'][] =['key','like','%'.$request->key.'%'];
        }
        $configs = $this->configRepository->paginateList(10,$conditions);
        return view('admin.pages.config.list', compact('configs','request'));
    }

    public function create()
    {
        return view('admin.pages.config.form');
    }

    public function store(ConfigRequest $request)
    {
        $this->handleSubmitRequest($request);

        return redirect()->route('config.index')
            ->with('message', 'Add successfull');
    }

    public function edit($id)
    {
        $config = $this->configRepository->findById($id);
        return view('admin.pages.config.form', compact('config'));
    }

    public function update(ConfigRequest $request, $id)
    {

        $this->handleSubmitRequest($request, $id);

        return redirect()->route('config.index')
            ->with('message', 'Edit successfull');
    }

    public function destroy($id)
    {
        $this->configRepository->destroy($id);

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }

    public function handleSubmitRequest($request, $id = null)
    {
        $attributes = $request->validated();
        $attributes['status'] = (int) $attributes['status'];

        if (!is_null($id)) {
            $this->configRepository->update($attributes, $id);
        } else {
            $this->configRepository->create($attributes);
        }

    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PermissionRequest;
use App\Http\Requests\RoleRequest;
use App\Repositories\Contracts\PermissionRepositoryInterface;
use App\Repositories\Contracts\RoleRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;

class RoleController extends Controller
{

    protected $permissionRepository;
    protected $roleRepository;

    public function __construct(PermissionRepositoryInterface $permissionRepository, RoleRepositoryInterface $roleRepository)
    {
        $this->permissionRepository = $permissionRepository;
        $this->roleRepository = $roleRepository;
    }


    public function index()
    {
        $roles = $this->roleRepository->paginateList();

        return view('admin.pages.role.index', compact(
            'roles'
        ));
    }

    public function create()
    {
        $permissions = $this->permissionRepository->all();
        $formattedRoles = [];
        foreach ($permissions as $permission) {
            if (!isset($formattedRoles[$permission->group])) {
                $formattedRoles[$permission->group] = [];
            }
            $formattedRoles[$permission->group][] = $permission;
        }
        return view('admin.pages.role.form', compact(
            'formattedRoles'
        ));
    }

    public function store(RoleRequest $request)
    {
//        dd($request->validated());
        $attributes = $request->validated();

        $this->roleRepository->create($attributes);

        Cache::forget('permissions');
        return redirect()->route('role.index');
    }

    public function edit($id)
    {
        $role = $this->roleRepository->findById($id);
        if (empty($role)) return redirect()->route('role.index');

        $permissions = $this->permissionRepository->all();
        $formattedRoles = [];
        foreach ($permissions as $permission) {
            if (!isset($formattedRoles[$permission->group])) {
                $formattedRoles[$permission->group] = [];
            }
            $formattedRoles[$permission->group][] = $permission;
        }
        return view('admin.pages.role.form', compact(
            'role',
            'formattedRoles'
        ));
    }

    public function update(RoleRequest $request, $id)
    {
        $attributes = $request->validated();
        $this->roleRepository->update($attributes, $id);

        Cache::forget('permissions');
        return redirect()->route('role.index');
    }

    public function destroy($id)
    {
        $this->roleRepository->destroy($id);

        Cache::tags('permissions')->flush();
        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }

    public function sync()
    {
        $groups = config('permissions');
        foreach ($groups as $group_name => $permissions) {
            $permission_ids = [];
            foreach ($permissions as $name => $permission) {
                $permission = $this->permissionRepository->updateOrCreate([
                    'name' => $name,
                    'group' => $group_name
                ], [
                    'name' => $name,
                    'group' => $group_name,
                    'routes' => $permission,
                ]);

                $permission_ids[] = $permission->id;
            }
            $this->roleRepository->updateOrCreate([
                'name' => $group_name,
            ], [
                'name' => $group_name,
                'permissions' => $permission_ids
            ]);
            unset($group_name);
        }
        Cache::tags('permissions')->flush();

        return redirect()->route('role.index');
    }
}

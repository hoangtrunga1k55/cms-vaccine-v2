<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CategoryRequest;
use App\Repositories\Contracts\CategoryRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\ImageHelper;
use Illuminate\Support\Str;


class CategoryController extends Controller
{
    protected $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function index(Request $request)
    {
        $condition = [];
        if (!is_null($request->status)) {
            $condition['where'][] = ['status', 'like', $request->status];
        }
        if (!is_null($request->name)) {
            $condition['where'][] = ['name', 'like', "%" . $request->name . "%"];
        }
        $categories = $this->categoryRepository->paginateList(10, $condition);
        return view('admin.pages.category.index', compact(
            'categories', 'request'
        ));
    }

    public function create()
    {
        $categories = $this->categoryRepository->all();
        return view('admin.pages.category.form', compact('categories'));
    }


    public function store(CategoryRequest $request)
    {
        $this->handleSubmitRequest($request);

        return response()->json([
            'message' => 'Tạo thành công',
            'status' => 'success',
            'url' => route('category.index')
        ]);
    }

    public function edit($id)
    {
        $condition = [];
        $condition['whereNotIn']['_id'] = [$id];
        $condition['where'][] = ['parent_id', 'not like', "%" . $id . ",%"];
        $categories = $this->categoryRepository->all(["*"], $condition);
        $category = $this->categoryRepository->findById($id);
        return view('admin.pages.category.form', compact(
            'category', 'categories'
        ));
    }

    public function update(CategoryRequest $request, $id)
    {
        $this->handleSubmitRequest($request, $id);

        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('category.index')
        ]);
    }


    public function destroy($id)
    {
        $this->categoryRepository->destroy($id);

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }


    public function handleSubmitRequest($request, $id = null)
    {
        $attributes = $request->validated();
        $attributes['slug'] = Str::slug($attributes['name']);
        $attributes['status'] = (int)$attributes['status'];

        if (isset($attributes['parent_id'])) {
            $parent = $this->categoryRepository->findById($attributes['parent_id']);

            $parent_id = "";
            if (!is_null($parent->parent_id)) {
                $parent_id .= $parent->parent_id;
            }
            $parent_id .= $parent->_id . ",";

            $attributes['parent_id'] = $parent_id;
        }
        if (!is_null($id)) {
            $this->categoryRepository->update($attributes, $request->id);
        } else {
            $this->categoryRepository->create($attributes);
        }

    }
}

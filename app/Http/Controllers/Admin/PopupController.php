<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ImageHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\PopupRequest;
use App\Http\Requests\VendorRequest;
use App\Repositories\Contracts\NewsRepositoryInterface;
use App\Repositories\Contracts\PopupRepositoryInterface;
use App\Repositories\Contracts\VendorRepositoryInterface;
use Illuminate\Http\Request;

class PopupController extends Controller
{

    protected $popupRepository;
    protected $newsRepository;

    public function __construct(
        PopupRepositoryInterface $popupRepository,
        NewsRepositoryInterface $newsRepository)
    {
        $this->popupRepository = $popupRepository;
        $this->newsRepository = $newsRepository;
    }

    public function index(Request $request)
    {
        $condition=[];
        if (!is_null($request->status)){
            $condition['where'][] = ['status','like',$request->status];
        }
        $popups = $this->popupRepository->paginateList(10,$condition);
        return view('admin.pages.popup.list',compact(
            'popups','request'
        ));
    }

    public function create()
    {
        $news = $this->newsRepository->all();
        return view('admin.pages.popup.form', compact('news'));
    }


    public function store(PopupRequest $request)
    {
        $this->handleSubmitRequest($request);

        return response()->json([
            'message' => 'Tạo thành công',
            'status' => 'success',
            'url' => route('popup.index')
        ]);
    }

    public function edit($id)
    {
        $news = $this->newsRepository->all();
        $popup = $this->popupRepository->findById($id);
        return view('admin.pages.popup.form', compact('popup', 'news'));
    }

    public function update(PopupRequest $request, $id)
    {

        $this->handleSubmitRequest($request, $id);

        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('popup.index')
        ]);
    }


    public function destroy($id)
    {
        $this->popupRepository->destroy($id);

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }


    public function handleSubmitRequest($request, $id = null)
    {
        $attributes = $request->validated();

        $image = ImageHelper::upload($request->file('thumbnail'), '/uploads/popup/');
        if ($image != ImageHelper::$NOTFOUND) {
            $attributes['thumbnail'] = asset($image);
        }

        if (!is_null($id)) {
            $this->popupRepository->update($attributes, $request->id);
        } else {
            $this->popupRepository->create($attributes);
        }

    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UpdateVaccineCalendar;
use App\Http\Requests\UpdateVaccineNote;
use App\Repositories\Contracts\PathogenRepositoryInterface;
use App\Repositories\Contracts\VaccineRepositoryInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;

class VaccineNoteController extends Controller
{
    protected $pathogenRepository;
    protected $vaccineRepository;

    public function __construct(VaccineRepositoryInterface $vaccineRepository, PathogenRepositoryInterface $pathogenRepository)
    {
        $this->vaccineRepository = $vaccineRepository;
        $this->pathogenRepository = $pathogenRepository;
    }

    public function index()
    {

        $vaccines = $this->vaccineRepository->all();
        $pathogens = $this->pathogenRepository->all();
        $note = [];

        if (file_exists(public_path('vaccine_note.json'))) {
            $note = file_get_contents(public_path('vaccine_note.json'));
            $note= json_decode($note, true);
        }

        return view('admin.pages.vaccine_note.form', compact(
            'vaccines',
            'pathogens',
            'note'));
    }

    public function save(UpdateVaccineNote $request)
    {
        $data = $request->validated();
        file_put_contents(public_path('vaccine_note.json'), json_encode($data));
        return $data;
    }
}

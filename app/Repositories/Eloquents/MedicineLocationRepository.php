<?php

namespace App\Repositories\Eloquents;

use App\Models\MedicineLocation;
use App\Repositories\Contracts\MedicineLocationRepositoryInterface;

class MedicineLocationRepository extends BaseRepository implements MedicineLocationRepositoryInterface
{
    function __construct(MedicineLocation $model)
    {
        $this->model = $model;
    }

}

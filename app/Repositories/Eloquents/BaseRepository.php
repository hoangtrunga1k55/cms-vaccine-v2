<?php

namespace App\Repositories\Eloquents;

use App\Models\Wallet\CmsLog;
use Faker\Provider\Uuid;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

//use Webpatser\Uuid\Uuid;

class BaseRepository
{
    /**
     * Eloquent model
     */
    protected $model;

    protected $cmsLog;

    protected $cmsType;

    protected $email;

    /**
     * @param $model
     */
    function __construct($model)
    {
        $this->model = $model;
    }

    public function count($conditions = [])
    {
        $query = $this->model->select('_id');

        $this->_buildQueryFromFilter($query, $conditions);

        return $query->count();

    }

    public function all($columns = array('*'), $conditions = [], $relations = [], $orders = [])
    {
        $query = $this->filter($conditions, $relations, $orders);
        return $query->get($columns);
    }

    public function paginateList($per = 10, $conditions = [], $relations = [], $orders = [])
    {
        $query = $this->filter($conditions, $relations, $orders);
        return $query->paginate($per);
    }

    public function filter($conditions, $relations, $orders)
    {;
        return $this->_buildQueryFromFilter(
            $this->model
                ->when(!empty($orders), function ($query) use ($orders) {
                    foreach ($orders as $field => $direction) {
                        $query = $query->orderBy($field, $direction);
                    }
                })->when(empty($orders), function ($query) {
                    $query->orderBy('created_at', 'desc');
                }), $conditions
        )->when(!empty($relations), function ($query) use ($relations) {
            $query->with($relations);
        });
    }

    public function first($conditions = [])
    {
        $query = $this->model;
        $query = $this->_buildQueryFromFilter($query, $conditions);
        return $query->first();
    }

    public function last($conditions = [], $order_by = null)
    {
        if (empty($order_by)) {
            $order_by = ['created_at' => 'desc'];
        }

        $query = $this->model->where($conditions);

        foreach ($order_by as $key => $row) {
            $query = $query->orderBy($key, $row);
        }

        return $query->first();
    }

    public function pluck($column, $key = null, $sortColumn = null, $direction = 'asc')
    {
        if (!empty($sortColumn)) {
            return $this->model->orderBy($sortColumn, $direction)->pluck($column, $key);
        } else {
            return $this->model->pluck($column, $key);
        }
    }

    public function findById($id, $columns = array('*'))
    {
        return $this->model->find($id, $columns);
    }

    public function findOneByConditions($conditions = [], $relations = [])
    {
        $query = $this->model->orderBy('created_at', 'desc');

        $query = $this->_buildQueryFromFilter($query, $conditions);

        if (!empty($relations)) {
            $query->with($relations);
        }

        return $query->first();
    }


    public function create($data)
    {
        $data['_id'] = (string) Str::uuid();

        return $this->model->create($data);
    }

    public function createMany($data)
    {
        return $this->model->insert($data);
    }

    public function update($data, $id)
    {
        $obj = $this->model->findOrFail($id);
        return $obj->update($data);
    }

    public function updateOrCreate($conditions, $data)
    {
        return $this->model->updateOrCreate($conditions, $data);
    }

    public function destroy($id)
    {
        $obj = $this->model->findOrFail($id);
        return $obj->delete();
    }

    public function findBy($key, $value, $relations = [])
    {
        $query = $this->model->where($key, $value);
        if (!empty($relations)) {
            $query->with($relations);
        }
        return $query->first();
    }

    public function findAllBy($key, $value, $relations = [])
    {
        $query = $this->model->where($key, $value);

        if (!empty($relations)) {
            $query->with($relations);
        }
        return $query->get();
    }

    public function updateAll($array_id, $data)
    {
        return $this->model->whereIn('_id', $array_id)->update($data);
    }

    public function findByListId($array_id)
    {
        return $this->model->whereIn('_id', $array_id)->get();
    }

    public function insert($array_data)
    {
        return $this->model->insert($array_data);
    }

    public function deleteList($arr_id)
    {
        return $this->model->whereIn('id', $arr_id)->delete();
    }

    public function deleteAll()
    {
        return $this->model->truncate();
    }

    public function deleteAllBy($conditions)
    {
        return $this->model->where($conditions)->delete();
    }

    public function sum(array $fields, $filter = [])
    {
        switch (count($fields)) {
            case 0:
                return 0;
            case 1:
                $query = $this->model->orderBy('created_at', 'desc');
                $query = $this->_buildQueryFromFilter($query, $filter);
                return $query->sum($fields[0]);
            default:
                $fields = implode(' + ', $fields);
                $query = $this->model->orderBy('created_at', 'desc');
                $query = $this->_buildQueryFromFilter($query, $filter);
                return $query->sum(DB::raw($fields));
        }
    }

    private function _buildQueryFromFilter($query, $filter = [])
    {
        if (!empty($filter)) {
            $query->where(function ($query) use ($filter) {
                if (isset($filter['whereIn'])) {
                    foreach ($filter['whereIn'] as $key => $arr) {
                        $query->whereIn($key, $arr);
                    }
                    unset($filter['whereIn']);
                }

                if (isset($filter['whereNotIn'])) {
                    foreach ($filter['whereNotIn'] as $key => $arr) {
                        $query->whereNotIn($key, $arr);
                    }
                    unset($filter['whereNotIn']);
                }

                if (isset($filter['orWhereIn'])) {
                    foreach ($filter['orWhereIn'] as $key => $arr) {
                        $query->orWhereIn($key, $arr);
                    }
                    unset($filter['orWhereIn']);
                }

                if (isset($filter['where'])) {
                    $query->where($filter['where']);
                    unset($filter['where']);
                }

                if (isset($filter['orWhere'])) {
                    foreach ($filter['orWhere'] as $row) {
                        $query->orWhere($row[0], $row[1], $row[2]);
                    }
                    unset($filter['orWhere']);
                }

                if (isset($filter['whereNull'])) {
                    foreach ($filter['whereNull'] as $row) {
                        $query->whereNull($row);
                    }
                    unset($filter['whereNull']);
                }

                if (!empty($filter['relations'])) {
                    foreach ($filter['relations'] as $relation => $filter_2) {
                        if (!empty($filter_2['whereHas'])) {
                            $_filter = $filter_2['whereHas'];
                            $query->whereHas($relation, function ($query) use ($_filter) {
                                $this->_buildQueryFromFilter($query, $_filter);
                            });
                        }

                        if (!empty($filter_2['orWhereHas'])) {
                            $_filter = $filter_2['orWhereHas'];
                            $query->orWhereHas($relation, function ($query) use ($_filter) {
                                $this->_buildQueryFromFilter($query, $_filter);
                            });
                        }
                    }
                    unset($filter['relations']);
                }
            });
        }

        if (!empty($filter['conditions'])) {
            foreach ($filter['conditions'] as $row) {
                $this->_buildQueryFromFilter($query, $row);
            }
        }

        return $query;
    }
}

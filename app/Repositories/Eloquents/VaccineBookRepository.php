<?php

namespace App\Repositories\Eloquents;

use App\Models\VaccineBook;
use App\Repositories\Contracts\VaccineBookRepositoryInterface;

class VaccineBookRepository extends BaseRepository implements VaccineBookRepositoryInterface
{
    function __construct(VaccineBook $model)
    {
        $this->model = $model;
    }

}

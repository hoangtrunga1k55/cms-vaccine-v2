<?php

namespace App\Repositories\Eloquents;

use App\Models\Tag;
use App\Repositories\Contracts\TagRepositoryInterface;

class TagRepository extends BaseRepository implements TagRepositoryInterface
{
    function __construct(Tag $model)
    {
        $this->model = $model;
    }
}

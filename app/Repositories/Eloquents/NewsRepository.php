<?php

namespace App\Repositories\Eloquents;

use App\Models\News;
use App\Repositories\Contracts\NewsRepositoryInterface;

class NewsRepository extends BaseRepository implements NewsRepositoryInterface
{
    function __construct(News $model)
    {
        $this->model = $model;
    }
}

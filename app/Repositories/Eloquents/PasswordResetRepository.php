<?php

namespace App\Repositories\Eloquents;

use App\Models\PasswordReset;
use App\Repositories\Contracts\PasswordResetRepositoryInterface;

class PasswordResetRepository extends BaseRepository implements PasswordResetRepositoryInterface
{
    function __construct(PasswordReset $model)
    {
        $this->model = $model;
    }
    public function getModel()
    {
        return PasswordReset::class;
    }
}

<?php

namespace App\Repositories\Eloquents;

use App\Models\VaccineCalendar;
use App\Repositories\Contracts\VaccineCalendarRepositoryInterface;

class VaccineCalendarRepository extends BaseRepository implements VaccineCalendarRepositoryInterface
{
    function __construct(VaccineCalendar $model)
    {
        $this->model = $model;
    }

}

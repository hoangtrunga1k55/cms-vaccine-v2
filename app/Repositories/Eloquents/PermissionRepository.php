<?php

namespace App\Repositories\Eloquents;

use App\Models\Permission;
use App\Repositories\Contracts\PermissionRepositoryInterface;

class PermissionRepository extends BaseRepository implements PermissionRepositoryInterface
{
    function __construct(Permission $model)
    {
        $this->model = $model;
    }
}

<?php

namespace App\Repositories\Eloquents;

use App\Models\Vaccine;
use App\Repositories\Contracts\VaccineRepositoryInterface;

class VaccineRepository extends BaseRepository implements VaccineRepositoryInterface
{
    function __construct(Vaccine $model)
    {
        $this->model = $model;
    }

}

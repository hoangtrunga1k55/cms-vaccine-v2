<?php

namespace App\Repositories\Eloquents;

use App\Models\Vendor;
use App\Repositories\Contracts\VendorRepositoryInterface;

class VendorRepository extends BaseRepository implements VendorRepositoryInterface
{
    function __construct(Vendor $model)
    {
        $this->model = $model;
    }
}

<?php

namespace App\Repositories\Eloquents;

use App\Models\Popup;
use App\Repositories\Contracts\PopupRepositoryInterface;

class PopupRepository extends BaseRepository implements PopupRepositoryInterface
{
    function __construct(Popup $model)
    {
        $this->model = $model;
    }

}

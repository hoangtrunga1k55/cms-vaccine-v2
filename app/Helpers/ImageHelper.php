<?php


namespace App\Helpers;


use CKSource\CKFinder\Filesystem\File\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ImageHelper
{
    static  $NOTFOUND = 'https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg';
    public static function upload($image, $path)
    {
        if (!is_null($image)) {
            $image_name = Str::uuid().'.'.$image->getClientOriginalExtension();
            $image->move(public_path($path),$image_name);
            $path .= $image_name;
            return $path;
        }
        return self::$NOTFOUND;
    }

    public static function getImage($image)
    {
        $isExists = file_exists(public_path($image));
        if($isExists){
            return asset($image);
        }
        else {
            return self::$NOTFOUND;
        }
    }
}

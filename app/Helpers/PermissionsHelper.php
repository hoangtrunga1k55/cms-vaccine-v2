<?php


namespace App\Helpers;

use App\Repositories\Contracts\PermissionRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;

class PermissionsHelper
{

    public static function get()
    {
        if (!Cache::has('permissions')) {
            $roles = Auth::user()->roles;
            $permissions_id = [];
            foreach ($roles as $role) {
                if (!is_null($role->permissions)) {
                    foreach ($role->permissions as $permission) {
                        $permissions_id[] = $permission;
                    }
                }
            }
            $permission = app(PermissionRepositoryInterface::class)
                ->findByListId($permissions_id)
                ->toArray();
            if (empty($permission)) {
                $filteredRoutes = [];
            } else {
                $filteredRoutes = array_unique(array_merge(
                    ...array_column($permission, 'routes')
                ));
            }
            Cache::add('permissions', $filteredRoutes);
        }
        return Cache::get('permissions');
    }


    public static function can($route): bool
    {
        if (in_array($route, PermissionsHelper::getAllRoutes())) {
            return in_array($route, PermissionsHelper::get());
        }
        return true;
    }

    public static function getAllRoutes(){
        $routeCollection = Route::getRoutes();
        $arr = [];
        foreach ($routeCollection as $route) {
            if (in_array('permission', $route->action['middleware']??[])) {
                $arr[] = $route->action['as'];
            }
        }
        return array_unique($arr);
    }
}

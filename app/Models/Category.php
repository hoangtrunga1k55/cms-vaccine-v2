<?php

namespace App\Models;

class Category extends MongoModel
{
    protected $collection = 'category';
    protected $guarded = [];

    public function getID(): string {
        return isset($this->id) ? $this->id :"" ;
    }

    public function getNewsIDs(): array {
        if (!is_array($this->news_ids)){
            return [] ;
        } ;
        return isset($this->news_ids) ? $this->news_ids :[] ;
    }


    public function news()
    {
        return $this->belongsToMany(News::class);
    }
}

<?php

namespace App\Models\Wallet;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class MongoModel extends Eloquent
{
    protected $connection = 'mongodb';
    protected $keyType = "string";
}

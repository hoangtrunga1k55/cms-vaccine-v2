<?php

namespace App\Models\Wallet;

use Illuminate\Database\Eloquent\Model;

class PromoUserVoucher extends Model
{
    protected $collection = 'promo_user_voucher';

    protected $fillable = [
        'id_voucher',
        'id_user',
        'amount',
        'total',
        'code',
        'used',
        'status',
        'expired'
    ];

}

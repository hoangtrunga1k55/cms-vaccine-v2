<?php

namespace App\Models\Wallet;

class MerchantVoucher extends MongoModel
{
    protected $collection = 'merchant_vouchers';
    
//    protected $primaryKey = '_id';

    protected $fillable = [
        '_id',
        'total_used',
        'merchant_id',
        'used',
        'voucher_code',
        'discount_percent',
        'discount_amount',
        'expired_at',
        'use_hold',
        'status',
        '__v'
    ];
}

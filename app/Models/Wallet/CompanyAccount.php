<?php

namespace App\Models\Wallet;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Foundation\Auth\Access\Authorizable;

class CompanyAccount extends MongoModel implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $collection = 'company_accounts';
    
    protected $primaryKey = '_id';

    protected $fillable = ['_id', 'email', 'is_two_factor', 'password', 'secret_key', 'company_id', '__v', 'last_login', 'salt'];

}

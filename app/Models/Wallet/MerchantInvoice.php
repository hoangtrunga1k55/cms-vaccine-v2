<?php

namespace App\Models\Wallet;

class MerchantInvoice extends MongoModel
{
    protected $collection = 'merchant_invoices';
    
    protected $fillable = [
        '_id',
        'invoice_id',
        'amount',
        'status',
        'user_id',
        'merchant_id',
        'currency',
        'voucher_code',
        'allow_voucher',
        'voucher_discount_amount',
        'amount_before_voucher',
    ];

    public function promo_voucher()
    {
        return $this->belongsTo('App\Models\Wallet\PromoVoucher','voucher_id', '_id');
    }
}

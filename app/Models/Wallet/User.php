<?php

namespace App\Models\Wallet;

class User extends MongoModel {

    protected $collection = 'users';

//    protected $primaryKey = '_id';

    protected $guarded = [];

    public function balancePrimary()
    {
        return $this->hasMany('App\Models\WalletCore\UserBalance', 'user_id', 'core_id')->where('balance_id', 'PRIMARY');
    }

    public function balances()
    {
        return $this->hasMany('App\Models\WalletCore\UserBalance', 'user_id', 'core_id');
    }

}

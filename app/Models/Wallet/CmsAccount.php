<?php

namespace App\Models\Wallet;

use Illuminate\Auth\Authenticatable;
use Jenssegers\Mongodb\Eloquent\Model as Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Maklad\Permission\Traits\HasRoles;

class CmsAccount extends MongoModel implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasRoles;

    protected $collection = 'cms_accounts';
    
//    protected $guarded = 'web';
//    protected $guard_name = 'web';

    protected $fillable = ['_id', 'is_two_factor', 'email', 'password', 'secret_key', 'salt', '__v'];

}

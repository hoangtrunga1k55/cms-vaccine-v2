<?php

namespace App\Models\Wallet;

class RequestWithdraw extends MongoModel
{
    protected $collection = 'request_withdraws';

    protected $fillable = [
        '_id',
        'amount',
        'type',
        'status',
        'state',
        'company_id',
        'company_account_email',
        'merchant_id',
        'merchant_account_email',
        'merchant_balance_id'
    ];
}

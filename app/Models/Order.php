<?php

namespace App\Models;

use App\Repositories\Contracts\MedicineLocationRepositoryInterface;
use App\Repositories\Contracts\PathogenRepositoryInterface;
use App\Repositories\Eloquents\MedicineLocationRepository;
use Illuminate\Support\Facades\DB;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Order extends MongoModel
{

    use SoftDeletes;

    protected $guarded = [];

    const STATUS = [
        0 => "Initial",
        1 => "Processing",
        2 => "Finish",
        3 => "Failed",
    ];

    const STATUS_INITIAL = 0;
    const STATUS_PROCESSING = 1;
    const STATUS_FINISH = 2;
    const STATUS_FAILED = 3;

    const STATE = [
        0 => "Applied",
        1 => "Waiting Payment",
        2 => "Waiting Verify Payment",
        3 => "Payment Error",
        4 => "Payment Timeout",
        5 => "Reject by Admin",
        6 => "Reject by User",
        7 => "Success",
    ];

    const STATE_APPLIED = 0;
    const STATE_WAITING_PAYMENT = 1;
    const STATE_WAITING_VERIFY_PAYMENT = 2;
    const STATE_PAYMENT_ERROR = 3;
    const STATE_PAYMENT_TIMEOUT = 4;
    const STATE_REJECT_BY_ADMIN = 5;
    const STATE_REJECT_BY_USER = 6;
    const STATE_SUCCESS = 7;

    public function orderInfo()
    {
        $item = DB::table("order_info_" . $this->mc_code)
            ->where("order_id", "like", $this->order_id)
            ->first();
        $location_medicine = app(MedicineLocationRepositoryInterface::class)
            ->findById($item['location_medical']);
        $item['location'] = $location_medicine ? $location_medicine->name : null;
        $pathogen = app(PathogenRepositoryInterface::class)
            ->findById($item['pathogen_id']);
        $item['pathogen'] = $pathogen ? $pathogen->name : null;

        return $item;
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function ward()
    {
        return $this->belongsTo(Ward::class);
    }
}

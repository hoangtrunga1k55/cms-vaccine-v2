<?php

namespace App\Models;

class VaccineBook extends MongoModel
{
    protected $collection = 'vaccine_book';

    protected $fillable = ['_id', 'name'];

    public function user_info() {
        return $this->belongsTo('App\Models\Wallet\User','user_id');
    }
    const GENDER = [
        'MALE' => "Nam",
        'FMALE' => "Nữ"
    ];

    public function getGender(){
        if(isset(self::GENDER[$this->gender])){
            return self::GENDER[$this->gender];
        }
        return "Không xác định";

    }
}

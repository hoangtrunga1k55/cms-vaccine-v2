<?php

namespace App\Models;

use App\Models\Wallet\User;

class Notification extends MongoModel
{
    protected $collection = 'notifications';
    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}

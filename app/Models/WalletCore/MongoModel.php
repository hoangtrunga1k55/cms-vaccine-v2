<?php

namespace App\Models\WalletCore;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class MongoModel extends Eloquent
{
    protected $connection = 'mongodb2';

    protected $primaryKey = '_id';

}

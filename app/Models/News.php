<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class News extends MongoModel
{
    use SoftDeletes;

    const DRAFT = 0;
    const WAIT_REVIEW = 1;
    const WAIT_RELEASE = 2;
    const RELEASE = 3;

    const STATUS = [
//        Xóa
//        0 => "Nháp",
        1 => "Chờ duyệt",
        2 => "Đã duyệt",
        3 => "Đã công bố"
    ];

    protected $collection = 'news';
    protected $guarded = [];

    public function getPublishedAt(): string
    {
        return isset($this->published_at) && !is_array($this->published_at) ? $this->published_at : "";
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function vendors()
    {
        return $this->belongsToMany(Vendor::class);
    }

    public function getStatus()
    {
        return self::STATUS[$this->status];
    }

    public function author()
    {
        return $this->belongsTo(CmsAccount::class, 'creator_id');
    }
    public function approve()
    {
        return $this->belongsTo(CmsAccount::class, 'approved_by');
    }
}
